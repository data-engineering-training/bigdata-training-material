# MapReduce

## Slide 1
Introduction

* Programming Abstraction to model computations implementation in Java/C++
* Inspired but not direct implementation of functional programming map-reduce/fold operations
* Abstracts algorithm execution to a number of worker nodes
* Defines computation operations without meddling with execution
* Tunable operations based on data attibutes/partitioning
* Similar more strict APIs available for running on a single node in almost all languages

# Slide 2
Programming for :

* Implementation of Iterative Numerical Algorithms
  * Machine Learning
  * Optimization
  * Linear Algebra etc
* Implementation of Relational Operators found in SQL
  * Joins
  * Selections
  * Projections
  * Aggregations
  * Custom Function Application

# Slide 3

* Data as File Input or a Queue is read and represented as a List of some kind of Objects
* Mapper :
  * accepts object one by one in each invocation
  * transforms object / applies computation / creates new object
  * returns single output
* Reducer (Hadoop API):
  * accepts a set of intermediate values which share a key to a smaller set of values
  * iterate on the list and return result per key
* ReduceByKey (Spark API):
  * accepts a tuple of objects
  * first object is an Accumulator second is the Value to work on
* Reduce (Spark API):
  * values in Spark need not be in (key,value) format after Mapper. Mapper can output any kind of Object
  * accepts a tuple of objects
  * first object is an Accumulator second is the Value to work on 
* Reduce operations should Commutative and Associative :
  * No guarantee about the order objects will arrive for processing
  * Data Modelling is essential for operations
  * Commutative : A+B = B+A
  * Associative : (A+B) + C = A + (B+C)
  * Not as Exotic as it Sounds , real world examples soon 

![MapReduce Logical Model](img/map_reduce_logic_diagram.png)


# Slide 4
Dataset presentation : 

* NYC yellow taxi routes data
* Fields :  VendorID, tpep_pickup_datetime, tpep_dropoff_datetime, passenger_count, trip_distance, RatecodeID, store_and_fwd_flag, PULocationID, DOLocationID, payment_type, fare_amount,extra, mta_tax, tip_amount, tolls_amount, improvement_surcharge, total_amount
* Reference File for Locations in NY : LocationID, Borough, Zone, service_zone

Sample data Routes : 

VendorID,tpep_pickup_datetime,tpep_dropoff_datetime,passenger_count,trip_distance,RatecodeID,store_and_fwd_flag,PULocationID,DOLocationID,payment_type,fare_amount,extra,mta_tax,tip_amount,tolls_amount,improvement_surcharge,total_amount
1,2017-01-09 11:13:28,2017-01-09 11:25:45,1,3.30,1,N,263,161,1,12.5,0,0.5,2,0,0.3,15.3
1,2017-01-09 11:32:27,2017-01-09 11:36:01,1,.90,1,N,186,234,1,5,0,0.5,1.45,0,0.3,7.25
1,2017-01-09 11:38:20,2017-01-09 11:42:05,1,1.10,1,N,164,161,1,5.5,0,0.5,1,0,0.3,7.3
1,2017-01-09 11:52:13,2017-01-09 11:57:36,1,1.10,1,N,236,75,1,6,0,0.5,1.7,0,0.3,8.5
2,2017-01-01 00:00:00,2017-01-01 00:00:00,1,.02,2,N,249,234,2,52,0,0.5,0,0,0.3,52.8
1,2017-01-01 00:00:02,2017-01-01 00:03:50,1,.50,1,N,48,48,2,4,0.5,0.5,0,0,0.3,5.3
2,2017-01-01 00:00:02,2017-01-01 00:39:22,4,7.75,1,N,186,36,1,22,0.5,0.5,4.66,0,0.3,27.96

Sample data Locations : 
"LocationID","Borough","Zone","service_zone"
1,"EWR","Newark Airport","EWR"
2,"Queens","Jamaica Bay","Boro Zone"
3,"Bronx","Allerton/Pelham Gardens","Boro Zone"
4,"Manhattan","Alphabet City","Yellow Zone"
5,"Staten Island","Arden Heights","Boro Zone"
6,"Staten Island","Arrochar/Fort Wadsworth","Boro Zone"

# Slide 5

SQL Operations with MapReduce: Selection and Projection
1. Step 1 Mapper Input : 
  
(1,2017-01-09 11:13:28,2017-01-09 11:25:45,1,3.30,1,N,263,161,1,12.5,0,0.5,2,0,0.3,15.3 )

Step 2 Mapper Processing : 
1. On previous tuple check values of field VendorId if equals 1 then produce/return (VendorId, passenger_count, total_amount)
  
(1, (1, 15.3))

Step 3 Reducer in this case is just Fan out:

1. (1, [(1, 15.3),(1, 7.25),... ])

# Slide 6
SQL Operations with MapReduce Group By and Aggregation:
Step 1 Mapper Input : 

1. (2,2017-01-01 00:00:00,2017-01-01 00:00:00,1,.02,2,N,249,234,2,52,0,0.5,0,0,0.3,52.8 )

Step 2 Mapper Processing : 

1. On previous tuple select/return (VendorId, passenger_count, total_amount)
   
(2, (1, 52.8))

Step 3 Reducer in this case you can apply any function on the list of Values the key points to:

1. (2, [(1, 52.8),(4, 27.96),... ])

Step 4 Reducer return value :

1. (2 , ( 1+4+... , 52.8 + 27.96... ))


# Slide 7
Execution model : 

* Each mapper is fed with partitions of data
* Mapper outputs values that are further suffled in memory or in disk
* Reducers are fed with new suffled partitions with one or more keys
* Reducers apply operations indipedently per key and then merge all output
* Mappers and Reducers can run as independent processes fed with data
* If a Mapper or a Reducer fails , master retries job in another node 
![MapReduce Execution](img/map_reduce_exec_diagram.png)

# HDFS

## Slide 1

* Distributed Filesystem
* Virtual layer over Linux filesystem
  * on HDFS 3.1 can virtualize other storage sources as well (eg cloud storage)
* Utilized through :
  * Direct shell access
  * programmatic API and HTTP access python/java/etc
  * indirectly through databases having it as a persistence engine.

## Slide 2

* Architecture
  * 1 or 2 Namenodes
  * 1 or more Datanodes
  * 3 journal nodes if HA mode is configured for Namenode
  * Essentially java processes running on linux servers and communicating over tcp sockets and a binary protocol 
  * File access is transparent to architecture and implementation details

## Slide 3

* Files can be replicated (3 copies default)
* Files split in blocks (size configurable , dependent on cluster size, file type, processing options)
* Files split over Datanodes in blocks
* Namenode is the catalogue of blocks/files assignments to Datanodes
  * Namenode metadata files are the most critical , if lost, filesystem is corrupted might lose data.

## Slide 4 

* File access directly through Datanodes once the client queries Namenode
* Http api and basic web app for monitoring, exposes metrics for more sophisticated monitoring/alerting
* fsck , rebalancing and safemode state are the minimum management operations 
* unix style access control

## Slide5
File Formats : 

* Parquet : Binary , Column store format with indexes in v2 hopefully and predicate pushdown option if used in Impala query engine
* ORC : Binary , Column store format with indexes , ACID semantics and predicate pushdown option for Hive query engine
* SequenceFile : flat files consisting of binary key/value pairs
* TextFile : textfile with column and line delimiters 
* RCFile : Binary file, optimized for table storage , supports row partitioning and column vertical partitions in a column storage way 

all formats support APIs for direct file generation
all formats are open standards maintained by Apache
careful with datatypes on each format , esp Timestamps, Date , Datetime , Time and Timezones
complex nested data structures supported in binary formats 

## Slide 6
Parquet format data model : 

![Parquet file layout](img/ParquetFileLayout.gif)


![Parquet data model](img/FileFormat.gif)


# HBase

## Slide1
Features : 

* Multi model database (Key-Value , Column Family)
* Horizontally Scalable
* Scalable across Data Centers
* Real time random read/write
* Scales to Billions of Rows x Millions of Columns x thousand of Concurrent connections
* Modelled after Google's BigTable database

## Slide2
Features :

* Linear and modular scalability
* Strictly consistent reads and writes
* Automatic and configurable sharding of tables
* Automatic failover support between RegionServers
* Block cache and Bloom Filters for real-time queries.
* Query predicate push down via server side Filters

## Slide3
Use Cases :

* Realtime random inserts
* Random Reads by key or key ranges
* Time series
* Key-Value blob storage
* Schemaless (bytes in , bytes out up to consumer/producer service to interpret)

## Slide4
Architecture : 

* HDFS as a storage layer
* Master Servers keeping metadata , at least 1 active , preferably odd number available to cope with failures
* Region Servers keeping table data , colocated with HDFS datanodes , add/remove on demand
* Zookeeper acting as metadata storage and coordination engine

![HBase Architecture](img/HBaseArchitecture-Blog-Fig1.png)

![HBase Architecture](img/HBaseArchitecture-Blog-Fig2.png)

## Slide5
Data Model 1 : 

* Namespace
* Table 
* Row
* Column Family 
* Column 
* Column Qualifier 
* Cell
* Timestamp

## Slide6
Data Model 2 : 

* Single Row Key ,lexicographically sorted with the lowest order appearing first in a table, data clustered on key
* Column Families : group of dynamically defined columns (bytes as well), Column Families must be predefined though
* Versioned Writes and Reads (configurable on the CF level)
* Column Families stored in different files
* No Secondary Indexes
* Row key determines sharding (either automatic or manual)
* TTL on records , removed on next compaction after TTL expiration
* Pagination Queries options 

## Slide7
Design and Operation Considerations : 

* Row key size can impact performance
* Region Split
* Region Compaction 
* Column family design can impact performance
* Delete only when REALLY needed , monitor compaction after deletes
* hbase:meta table maintained on Masters
* Master election through Zookeeper
* Zookeeper contains metadata on running system (region servers , active master etc)

## Slide8
Programming HBase :

* Java / Python / .net API
* JRuby shell
* Web based tools
* Query through Impala
* MapReduce jobs reading/writing directly
* Spark jobs 

## Slide9
HBase Data Model Operations : 

* Put
* Get
* Scan 
* Delete ( Forget It Was Ever Mentioned :) )
* Java Hadoop API and tool exist for fast Batch loads

## Slide10
ACID Semantics : 
* Not an ACID compliant database

Guarantees Provided :

* Atomicity, mutations are atomic within a row , no multirow transactional guarantees
* checkAndPut API (CAS like operation) to implement MVCC
* The order of mutations is seen to happen in a well-defined order for each row, with no interleaving
* Scans , Read Commited isolation at least
* A read will never return data that has not been made durable on disk (hflush on WAL , not on table file)

## Slide11
Add more about coordination , failures :

* Masters server metadata to clients and region servers
  * which table region is served by which RegionServer
  * key ranges that each table region serves
  * Reallocates table regions in case of new or failed RegionServers
  * decide on region splits
* Data is served directly from RegionServers
* Secondary indexes can be created as new tables via periodic inserts/dual writes
* Table region allocation to RegionServers critical for scalability and avoiding hotspots
* Key design important to avoid hotspots and loaded servers
* Tables are closed when Master believes not enough Region servers are available 
  (soon after HBase shuts down as well...YouAreDeadException)
* BlockCache can pin recently used data in memory and scale Reads , CFs can also be pinned in BlockCache 
* Disabling BlockCache on the table level can help performance as well for tables used in batch jobs/read once.
* Data are replicated through HDFS ,after a failed RegionServer the new one is reading from HDFS the file replicas
* Regions can be replicated for tunable HA reads 

## Slide 12

Key Value Model : 

|    Key    | ColumnFamily1:c1 |
|    ---    |:----------------:|
| ORD23331A | {orderItem:1 , orderProductDesc : 12} |
| ORD23332B | {orderItem:2 , orderProductDesc : "Fine Description"} |
| ORD23333C | {orderItem:3 , orderProductDesc : 124444} |

TimeSeries Data :

| Key | ColumnFamily1:temp | ColumnFamily2:cpu | ColumnFamily2:memory |
|    ---    |:----------------:|:----------------:|:----------------:|
|  Sensor1:TS1 |  18      | | |
|  Sensor1:TS2 |               | 95 | |
|  Sensor1:TS3 | 17       | 34 | |
|  Sensor1:TS4 | 19       | 38 | |
|  Sensor2:TS1 | | | 0.87 |
|  Sensor2:TS2 | 18 | | 0.65 |

Salting key : bucket = timestamp % numRegionServers
or Randomization through hashing

| Key | ColumnFamily1:value  |
| --- |:--------------------:|
| bucket:ts1:hostname | logMessage |
| bucket:ts2:hostname | logMessage |
| bucket:ts3:hostname | logMessage |

Reverse Timestamp : rsTimestamp = Long.MAX_VALUE – timestamp

| Key | ColumnFamily1:value  |
| --- |:--------------------:|
| VISA:rsTimestamp1 | ISO8583_BLOB_1 |
| VISA:rsTimestamp2 | ISO8583_BLOB_2 |
| MASTERCARD:rsTimestamp3 | ISO8583_BLOB_3 |

# Hive

## Slide1 

* SQL query engine over Hadoop
* Initially translated SQL queries to map/reduce jobs
* Manages tables/views
* latest versions provide ACID compliance
* SQL-92 compliance
* Runs TPC-C99 all benchmarks successfully
* Each query is managed through yarn (mostly)

## Slide2

* Work with Hive :
  * Web base workbenches (eg Hue , Hortonworks Hive Editor)
  * ODBC/JDBC connection 
  * Beeline command line tool
  * Compatible with all major Reporting/Dashboarding tools

## Slide3

* Hive decouples : 
  * storage 
  * resource management 
  * execution engine 
  * metadata management

* Execution Engines :
  * Map/Reduce
  * Tez
  * Spark

* Hive scalability depends on execution model
* Supported File Formats and Compression:  RCFile, Avro, ORC, Parquet; Compression, LZO

## Slide4

* HDFS to fetch data 
* Yarn to allocate resources
* HiveServer2 tcp server accepting HiveQL/SQL queries
* HCatalog table and storage management layer
* Metadata server manages tables/views metadata in a database (e.g. Derbydb,Postgresql,Mysql)
* All servers must be up and running for Hive to operate 


## Slide5
* Rich Datatypes 
  * Numeric Types
  * Date/Time Types (carefull with timezones)
  * String Types
  * Misc Types
  * Complex Types (Array , Map , Struct)
* Table Statistics 
* Explain statement with Query plan depending on execution engine
* Partitioning
* Hive Commands (non-SQL , executed on Beeline)
* Features heavily depend on Hive version

## Slide6

Hive Programming

* Tables/Views/Users
* HiveQL/SQL DDL 
* HiveQL/SQL DML
* UDFs already defined and custom 
* Mathematical/Analytical functions
* File management commands (LOAD etc)
* Low level streaming ingestion API


## Slide 7 

External and Managed Tables :

* On External , Hive handles only metadata and statistics
* Managed tables fully managed by Hive (files, metadata and statistics)
* A managed table is stored under the hive.metastore.warehouse.dir property and external wherever the file is defined
* External tables can refer to files Azure Storage Volumes (ASV) or remote HDFS locations, apart from local HDFS

Hive Partitioning :

* A table can have one or more partition columns 
* Separate data directory is created for each distinct value combination in the partition columns
* Carefull when defining partitioned columns
* Tables or partitions can be bucketed using CLUSTERED BY columns 
* bucket number is determined by the expression hash_function(bucketing_column) mod num_buckets
* Data can be sorted within that bucket via SORT BY columns

## Slide 8

Hive Transactions :

* Only for Managed tables , not for External
* Single Table transactions for INSERT/DELETE/UPDATE
* Table should be marked transactions on creation
* Currently only for ORC files , soon for other formats
* UPDATE means create delta files in other directories , then COMPACT (manual or auto)
* Updates and deletes perform full partition scans
* Only SNAPSHOT Isolation is available

## Slide 9

LLAP Engine ( Live Long and Process ):

* Long running processes working with TEZ and YARN
* HiveServer2 still decides the query plan , creates tasks
* Daemons prefetch and cache data on column level
* Multithreaded operator pipelines for query processing and security
* Data cached off-heap
* Small/short queries are largely processed by this daemon directly
* Or spawn YARN containers
* Tez schedules and monitors tasks executed by LLAP or Yarn Containers
* An LLAP Daemon allows parallel execution for multiple query fragments from different queries and sessions


# Impala


## Slide1 
* SQL query engine over Hadoop
* Manages tables/views
* SQL-92 compliance
* Runs TPC-DS most benchmarks successfully and beats all SQL on Hadoop engines
* Reuses Hive and Hadoop services, optimizes on SQL execution

## Slide2
* Work with Impala :
  * Web base workbenches (eg Cloudera Hue and DS Workbench)
  * ODBC/JDBC connection 
  * impala-shell command line tool
  * Compatible with all major Reporting/Dashboarding tools
  * Higly compatible with HiveQL 

## Slide3
* HDFS/Amazon S3/Hbase/Kudu to fetch data 
* Resource allocation :
  * Static Service Pools , depending on static vs adhoc query load 
  * Dynamic Resource Pools within Service for admission control
  * Users assigned to Dynamic Resource pools
* Hive Metadata server manages tables/views metadata in a database (e.g. Derbydb,Postgresql,Mysql)
* Impala Catalog connects to Metastore to fetch/cache and serves metadata to Impala Servers
* Impala Statestore health check of Impala Server daemons on all the HDFS DataNodes in a cluster
* Impala Server (Query Plan/Coordination/Execution) tcp server accepting SQL queries
* All servers must be up and running for Impala to operate correctly

## Slide4
* Impala decouples : 
  * storage 
  * resource management 
  * execution engine 
  * metadata management

* Impala scalability depends on available Impala Servers , execution scales horizontaly
* Supported File Formats and Compression:  RCFile, Avro, SequenceFile, Parquet , Text; Compression, LZO, Bzip2, Snappy
* Execution engine optimized for Parquet Files
* Query Engine for Kudu (supporting update/delete)
* Query Engine for HBase (carefull with that)
* Load Balancer/Proxy to manage connections
* Dedicated roles for Impala Servers (coordinators) along with Resource management.


## Slide5
* Rich Datatypes 
  * Numeric Types
  * Timestamp ONLY , no Date/Time Types (carefull with timezones)
  * String Types
  * Complex Types (Array , Map , Struct)
* Table Statistics 
* Explain statement with Query plan depending on execution engine
* Partitioning
* Impala Commands (non-SQL , executed on impala-shell or sent over ODBC/JDBC session)

## Slide6
Impala Programming
* Databases/Schemas/Tables/Views/Roles
* SQL DDL 
* SQL DML
* Metadata commands and SQL Optimizer Hints 
* UDFs already defined and custom (C++/Java)
* Mathematical/Analytical/String functions
* File management commands (LOAD etc)

## Slide 7 
External and Internal Tables :
* On External , Impala handles only metadata and statistics
* Internal tables fully managed by Impala (files, metadata and statistics)
* An Internal table is stored under the designated Impala work area and an External wherever the file is defined
* External tables can refer to files Amazon S3/Kudu/HBase apart from local HDFS

Impala Partitioning :
* A table can have one or more partition columns and define expressions on values
* Separate data directory is created for each distinct value combination in the partition columns
* Table can have data in different file format per partition
* Carefull with REFRESH and COMPUTE STATS on partitioned tables (preferably run per partition)

# Spark

## Slide1
Spark : A unified analytics engine for large-scale data processing

* Apache hosted project with several commercial PaaS offerings
* Easy to deploy even without other Hadoop Infrastructure
* Programming API in Java/Scala/Python/R/SQL
* Unified API for Batch and Streaming computations

## Slide2 

Spark Components : 

* Core
* Streaming
* SQL
* ML
* GraphX

Spark can read from several common Database and streaming sources
Easy to extend and add sources/sinks 
Same Programming model for both local/cluster mode and batch/streaming 

## Slide3 

Spark Programming Models : 

* RDDs : Immutable Distributed Structures with operations (transformations/actions)
* Datasets : Typed RDDs 
* DataFrames : Optimized Distributed structure with operations and more efficient execution plan
* SQL over DataFrames : running SQL Statements over Dataframes

## Slide4

Spark Sources/Sinks: 

* Filesystems (HDFS/S3/local)
* Any kind of file type (text or binary)
* JDBC databases , HBase , Solr 
* Queues/Journals Kafka , AMQP based 
* New DataSource API and old RDD api easy to extend and take advantage of operators.

## Slide5

Spark Structures can be used to :

* Join data from different sources
* Created new columns and or Structures from operations
* Persist intermediate operations for reuse 
* Persist output of operations to a Database or Filesystem
* Use advanced analytics algorithms as intermediate computation steps
* Take advantage of diverse Structures for the problem at hand 
* Run Machine Learning Algorithms
* Run Machine Learning Pipelines

## Slide6

Spark Development cycle :

* Develop locally or on Notebook
* Run/Debug/Test on sample dataset locally on local mode
* Produce artifacts and deploy to HDFS
* Submit Spark Job on job server (via cmd or other tool)
* Monitor cluster UI and/or logs 
* Same code can be submitted in local or cluster mode 

## Slide7

Spark Architecture : 

* Spark Master : creates and monitors Spark jobs
* Spark Workers : starts executors for Spark jobs
* Spark Application : 1 Spark Driver + Many Spark Executors
* Spark Driver : Entry point for every Spark job 
* Spark Executors : Many per node , jvms executing transformations 
* SparkContext or SparkSession one per Application

## Slide8

Spark Application Execution : 

* Application is Submitted and scheduled by the Cluster Manager with resources request for Driver and Executors
* The Program runs on the Driver
* Driver splits work to Executors (static or dynamic)
* Each Action defined in the program defines a new Job for the Application
* Jobs are split on Stages that combine data from different executors 
* Stages consist of Tasks running of executors
* Number of Stages and Tasks depends on partitioning and on the sequence of transformations and actions
* Execution Plan is generated for each Job as a DAG of Stages for RDDs or Steps from SQL optimizer for DataFrames

## Slide9
Spark SQL : 

* Run HIVE instance with Spark as backend engine to utilize Spark Workers
* Reuse HIVE Metastore objects
* Run SQL from the Spark Shell
* Run SQL on Dataframes in a Spark Job
* Use JdbcRDD to connect to external Database using SQL and bring data in the SparkSession

# Yarn

## Slide1

Yet Another Resource Manager (YARN):

* Core Hadoop project
* Entry point to submit applications in a Hadoop Cluster
* Works with MapReduce/Spark/Hive
* Creates and isolates application containers to ensure applications run and complete
* Multidimensional and flexible Resource Management with Configuration and Constraints
* Job Scheduling/Monitoring 

## Slide2

Resource Manager allocates :

* vcpu
* memory
* disk i/o
* network
* GPUs (v3.1)
* FPGAs (v3.1)

Scheduler :

* pluggable and configurable Policies
* Preemption option (just forget it)
* Federation for huge clusters
* Resource reservation option

Option to run docker containers on v3.1

## Slide3

Architecture : 

* Resource managers :
  * more than one for HA
  * run on Master nodes
  * Arbitrates resources among all the submitted applications 
  * Consists of a Scheduler and an ApplicationsManager
  * HA mode through Zookeeper
* NodeManagers
  * Running on each worker machine
  * spawns containers to run applications submitted by Resource managers
  * monitors resource usage
  * reports on Resource manager
* Timeline Server
  * Storage and retrieval of application’s current and historic information 
  * Running attempts , container configuration , context etc

## Slide4

Application Submittion/Monitoring through :

* command line tools with access to edge or cluster servers
* REST API
* Web interface 
* ETL Orchestration tools (Oozzie , AirFlow)
* artifacts should be somewhere in HDFS 

## Slide5

Scheduling :

* Hierarchical Queues group resources and set guarantees/constraints
* Per Queue ACLs
* Priority Scheduling
* Absolute and Percentage based resource configuration on Queues
* Flexible Elasticity configuration in underutilized clusters
* Users are mapped to Queues and submitted applications negotiate resources
* Users and Groups can be mapped many times and priority is considered on submission
* Queue can be set explicitly on application submission
* Default resource calculator takes in to account only memory requirements 
* Dominant resource calculator takes both cpu and memory and optimizes for each user based on application requirements
* cpu based resource allocation needs CGroups enabled on Linux 

## Slide6

Application acceptance and allocation depends on :

* Queue capacity
* Per queue MB / VCore allocations
* User limit factor..can expand allocation beyond limits if available resources
* User weights within queues
* Minimum User Limit Percent
* Max per queue applications
* Max application lifetime..you still need to profile your applications
* Application priorities

## Slide7
Reservation

* Reserve resources over (and ahead of) time for critical jobs
* Provides guarantees over absolute amounts of resources
* Think cluster utilization and job profiles before you enable it

Preemption

* If Enabled, Queues cannot serve request as left with no resources after other Queues elastically expanded
* Let High Priority applications run if resources are already allocated to other tasks from other Queues
* kill lower priority applications as a last resort , iteratively as Application Masters to release containers
* Carefull, killing long running jobs close to termination might blow your etl planning

## Slide8
Example Yarn Configuration

How to monitor as a user


# Oozie

## Slide 1
* Server-based Orchestration tool/ Workflow Engine bundled with Cloudera distribution
* Java Web-Application that runs in a Java servlet-container.
* Specialized in running workflow jobs with actions that execute Hadoop Map/Reduce and Pig jobs
* Workflow is a collection of actions (i.e. Hadoop Map/Reduce jobs, Pig jobs) arranged in a control dependency DAG (Direct Acyclic Graph). 
* "Control Dependency" from one action to another means that the second action can't run until the first action has completed.

## Slide 2

Architecture

* Oozie Server deployed as Java Web Application hosted in a Tomcat server 

* All of the stateful information such as workflow definitions, jobs,  etc, are stored in a database (DerbyDB, HSQL, Oracle,  MySQL, or PostgreSQL).

* Oozie Client submits work, either via a CLI, and API,  or a web service / REST.

* Tightly coupled with YARN / HDFS / MapReduce

_IMG with oozie architecture in HA_


## Slide 3
Defining Workflows

Written in hPDL (a XML Process Definition Language similar to JBOSS JBPM jPDL).

DAG with 
* Control Flow Nodes: Start, End, Decision, Fork, Join, Kill or 
* Action Nodes: Map-Reduce, Pig, HDFS, SSH, Sub-Workflow, Java 
* Preparation (e.g. initialize or cleanup HDFS structures) ,Environment and Configuration xml tags

Nodes are connected by transitions arrows.

**! Cycles not supported** 

_Example of a Workflow_

## Slide 4
``` 
<workflow-app xmlns='uri:oozie:workflow:0.3' name='shell-wf'>
    <start to='shell1' />
    <action name='shell1'>
        <shell xmlns="uri:oozie:shell-action:0.1">
            <job-tracker>${jobTracker}</job-tracker>
            <name-node>${nameNode}</name-node>
            <configuration>
                <property>
                  <name>mapred.job.queue.name</name>
                  <value>${queueName}</value>
                </property>
            </configuration>
            <exec>${EXEC}</exec>
            <argument>A</argument>
            <argument>B</argument>
            <file>${EXEC}#${EXEC}</file> <!--Copy the executable to compute node's current working directory -->
        </shell>
        <ok to="end" />
        <error to="fail" />
    </action>
    <kill name="fail">
        <message>Script failed, error message[${wf:errorMessage(wf:lastErrorNode())}]</message>
    </kill>
    <end name='end' />
</workflow-app>

```



```
<workflow-app xmlns=’uri:oozie:workflow:0.5′ name=’SparkWordCount’>
      <start to=’spark-node’ />
        <action name=’spark-node’>
          <spark xmlns="uri:oozie:spark-action:0.1">
            <job-tracker>${jobTracker}</job-tracker>
            <name-node>${nameNode}</name-node>
            <prepare>
              <delete path="${nameNode}/user/${wf:user()}/${examplesRoot}/output-data"/>
            </prepare>
            <master>${master}</master>
            <name>SparkPi</name>
            <class>org.apache.spark.examples.SparkPi</class>
            <jar>lib/spark-examples.jar</jar>
            <spark-opts>--executor-memory 20G --num-executors 50</spark-opts>
            <arg>value=10</arg>
          </spark>
          <ok to="end" />
          <error to="fail" />
        </action>
        <kill name="fail">
          <message>Workflow failed, error
            message[${wf:errorMessage(wf:lastErrorNode())}] </message>
        </kill>
        <end name=’end’ />
        <sla:info>
          <sla:nominal-time>${nominal_time}</sla:nominal-time>
          <sla:should-start>${10 * MINUTES}</sla:should-start>
          <sla:should-end>${30 * MINUTES}</sla:should-end>
          <sla:max-duration>${30 * MINUTES}</sla:max-duration>
          <sla:alert-events>start_miss,end_miss,duration_miss</sla:alert-events>
          <sla:alert-contact>joe@example.com</sla:alert-contact>
        </sla:info>
    </workflow-app>
```

Oozie Client starts a workflow:
* CLI
* Client API
* Web Services API
* Self contained applications (saved in HDFS mandatory)

_Jobs Lifecycle: PREP, RUNNING, SUSPENDED, SUCCEEDED, KILLED, FAILED_


**Jobs are run under specific user configuration that is propagaged to all actions**
Careful in kerberized setups

**No workflow priority set by Oozie** : 
* Action is triggered and submitted for run, 
* Priority pushed to systems via configuration if the systems support it

## Slide 5
Workflow recovery (Re-runs) and User-Retries

Option to rerun workflow nodes that failed indepedently of Job Lifecycle Status.

**!! All input data used by a workflow job is immutable for the duration of the workflow job**

Workflow recovery:
* Manual re-run of the job. The user has to make sure everything is cleaned up prior to resubmitting the recovery workflow and
* The workflow state has to be Succeeded, Failed or Killed
* The workflow can resume to the last successful action BUT
* The user has to mark the successfully ran nodes to be skipped!

User Retries:
* Set up how many times each action can be rerun after reaching an ERROR or FAILED state (Killed).
* If the max retries is reached then the action and the workflow is considered as FAILED or ERROR.

Failures and Restarts depended on Action type:
* eg Map task can be restarted by map/reduce engine on YARN
* transient failures (eg network) can be handled by oozie up to a point.

## Slide 6
Oozie Coordinator System:

Allows the user to define and execute recurrent and interdependent workflow jobs (data application pipelines) called Coordinator applications

Run Workflow Jobs based on:
* Regular time intervals and/or 
* Data availability or
* Triggered by an external event.

Conditions that trigger a workflow job are modeled as a predicate

Data involving different Timezones ??  
1. Engine works only in UTC
2. Oozie has built-in functions for Date/Time manipulation that facilates the process

## Slide 7
Coordinator Application definition is consisted of:

1. Dataset definition 
2. Coordination application parameters

expressed in XML format and stored in HDFS

Synchronous Applications? ->  Its coordinator actions are created at specified time intervals.

Submit applications at the Oozie Coordinator Engine via CLI or Web Services API.

Coordinator job possible status: PREP, RUNNING, RUNNINGWITHERROR, PREPSUSPENDED, SUSPENDED, SUSPENDEDWITHERROR, PREPPAUSED, PAUSED, PAUSEDWITHERROR, SUCCEEDED, DONEWITHERROR, KILLED, FAILED

## Slide 8
Oozie Bundle System:

Higher-level abstraction that batch a set of coordinator applications

The user will be able to:

**start / stop / suspend / resume / rerun**

in the bundle level -> a better and easy operational control

*XML* used for bundle setup and parameterization

All the data related to the Bundle have to be located in HDFS

Bundle job status: 
PREP, RUNNING, RUNNINGWITHERROR, SUSPENDED, PREPSUSPENDED, SUSPENDEDWITHERROR, PAUSED, PAUSEDWITHERROR, PREPPAUSED, SUCCEEDED, DONEWITHERROR, KILLED, FAILED

User details are propagated to all children jobs

**Rerun** bundles for specific dates & times or run a subset of coordinator apps

# Airflow

## Slide 1
Airflow is a platform to programmatically author, schedule and monitor workflows.

Python based on Linux platforms

Workflows are modeled as Directed Acyclic Graphs (DAGs) via code

Web based UI to monitor and troubleshoot the data pipelines/ workflows

**_Principles:_**

- **Dynamic** pipeline generation via coding
- **Extensible**: define operators, executors and extend the library.
- **Elegant**: Lean and explicit pipelines. Parameterization through Jinja templating engine.
- **Scalable**: Modular architecture, usage of queues for orchestrating an arbitrary number of workers.

## Slide 2

Airflow Architecture

1. Master Node:
  - WebServer
  - Scheduler

2. Worker Node: Executors

3. Queuing services: Mesos, RabbitMQ etc.
  - One Default Queue or 
  - Configure one queue per worker
4. Airflow Metastore DB: MySQL, Postgres etc.

_Airflow Architecture Pic_

## Slide 3
What is a DAG file?

Code defining the pipeline:
- Configuration of the pipeline (execution frequency, retries, alerts on failures, name of the pipeline) 
- Setup Tasks as **Operators** (BaseOperator BashOperator, HiveOperator, Docker Operator etc.) or **Sensors** (ExternalEvent, HdfsSensor,SqlSensor, HTTPSensor etc.)
- Organize Operators by setting up dependencies between Operators and formulating the DAG
- Operators on the same DAG can be assigned on different Queues 
- Remote workers can accept tasks from different queues

NO ACTUAL DATA PROCESSING made by the DAG

Trigger Rules specify if and when to fire tasks in a chain :
* all_success
* all_failed
* all_done
* one_failed
* one_success
* dummy

More Functionality :
* Branch and Join operator
* Hooks
* Pools for resource management and to limit parallelism
* Connections for databases to reuse in Tasks
* Queues to limit parallelism , distribute tasks and constraint what is executed where
* XComs let tasks exchange messages , only for control
* SubDags

Sensors Available :
* ExternalTaskSensor , wait for another task to a diff DAG
* HDFSSensor
* HivePartitionSensor
* HttpSensor with flexible response handling
* SQLSensor
* S3 Sensors 
* TimeSensor

Operators Available :
* AWS
* BigQuery
* Cassandra
* DataBricks
* JiraOperator 
* Kubernetes Pod
* Shell
* Python
* SQL/JDBC Operators
* Sftp
* SparkJDBCOperator / SparkSQL / SparkSubmit
* Sqoop
* SSH
* Email
* CheckOperator to run a query against a DB
* Hive
* MsSqlToHiveTransfer
* Oracle 
* Http


## Slide 4 
Deploying DAGs 

1. Save them on Airflow's DAG folder
2. Run the Code
3. Validate Metadata
4. Testing the DAG (on a specific date)
5. Backfill (if necessary)
6. Merge code into a master scheduler code repository or through CLI and/or Web based UI

<!-- Run a tutorial by setting up the DAG and showcase the Web UI and the CLI. Also through the tutorial present them with 1-3 Operators. -->

# Data Lake Design

## Slide 1
Some History!

EDW and OLAP:
- No realtime analytics
- Tough to maintain
- Time consuming to add new sources

Appliance based columnar store Databases (Oracle,IBM Netezza, Microsoft, SAP, Teradata and HP Vertica):
- Low Usability (specialty hardware & software to set up, configure and tune)
- High Cost both on initial investments and capacity adding
- Single-Box scalability
- Batch Ingestion

What about:
Large Data Volumes of High Velocity? 
Data Lakes!
- Columnstore format
- Industry-standard hardware
- Easily scalable
- Separation of storage from query compute nodes
- Support of Real-Time Data Ingestion and Analytics

![Data Lake](img/DW_DataLake.JPG)

## Slide 2
**Data lake** is a data store built for the ingestion and processing of any raw data from multiple sources without prior structuring to a preferred model. In this store, data can be accessed, formatted,processed, and managed as required for business or technical purposes.

_3 key attributes:_

1. Collect everything
2. Dive in anywhere
3. Flexible access

Pitfalls:

 - Data Lake becomes just a dumping/staging area
 - No Lineage or blueprints from Raw Data to Information
 - No Space Management and Retention policies
 - Data Silos and data duplication ?
 - Governance?

 How to avoid them? --> Layered Data Lake! 
 ![Layered Data Lake](img/DataLakeZones.png)

## Slide 3
Layered Data Lake :

_1. Transient Zone / Landing Tier:_ Temporary storage of source data
 - Input: Sources like RDBMS, files, Structured/Unstructured, Streaming, External etc.
 - Output: Data Transfered to Raw Tier
 - Limited Access (Consumers mostly IT, Data Stewards)
 - Processes: Data Transfer and Intake, Discover Metadata, Operational Metrics & Monitoring, Data Validation & Clean up, Tokenization/Masking
 - Data Retention Policies: shortlived data

_2. Raw Data / Raw Tier_ : Original Source Data
 - Input: Landing Tier
 - Output: Trusted Zone & Sandboxing
 - Basic Validation and privacy
 - Role based access (Metadata available to all for exploration) 
 - Single Source of Truth with History
 - Processes: Register & Update Catalog, Operational Metrics & Monitoring, Data Transformations required for Single View of Truth, Tokenization/Masking
- Data Retention Policies are based on Governance needs

_3. Trusted Data /  Gold Tier _: Standardized on corporate governance/ quality policies
 - Input: Raw Data
 - Output: Refined Data
 - Single Version of Truth
 - Role based access (Metadata available to all for exploration) 
 - Processes: Register and Update Catalog, Build up Master Data Table for ML
 - Data Retention Policies are based on Domain needs

_4. Refined Data / Access Tier_ : Data Required for specific Domain Views transformed from existing data
 - Input: Output from Trusted and/or Raw Zone
 - Output: Saved back to Refined or pushed to applications like BI Tools or to Sandbox
 - Role based access (Metadata available to all for exploration) 
 - Processes: Domain specific transformations (Aggregations,Denormalizations), Model Building for repors, Detokenization based on access
 - Data Retention Policies are based on Use Case needs

_5. Sandboxing / Working Tier_ : Data Required for Model Building and Machine Learning
 - Input: Raw/ Refined/ Trusted Data
 - Output: Model that can be later on operationalized 
 - Processes: Data Science Analytics
 - Data Retention Policies: Based on Use Case lifetime
 
## Slide 4
Data Lake Best Practices:

_Define Multi-Tenant Data Lake Achitecture **Across** Domains_

_Data Intake Management & Monitoring:_
 - Use Metadata Management system to register Data Sources, Types, destination and transport mechanism
 - Reusable components for Data validation, standardization and format conversions
 - Repeatable Pipelines with ability to take in full/ incremental loads
 - Operational stats on ingestion

_Data Governance to avoid Data Swamps_
  - Data Catalog
  - Data Set Tagging
  - Ability for crowdsourcing, description and additional attributes on metadata
  - End-to-End Lineage

_Self Service Data Lake to increase adoption and business value and maintain Governance_
 - Provision Sandboxing
 - Support repeatability for provisioning data to Sandboxes
 - Policies to govern space, access to data and archival after project completion


# Big Data Algorithms

## Slide 1

- HyperLogLog

- LSH

- Bloom-Filters

# Big Data Algorithms

## Slide 1
Common Algorithms applied on Big Data?

Problems with **Time** and **Space** Complexity

Even the count-distinct problem has Increased Complexity.

Need for probabilistic Data Structures and Algos that address these issues

## Slide 2 
HyperLogLog

Approximating the number of distinct elements in a multiset.

Why is it needed? --> Calculating the cardinality requires memory proportional to the cardinality

The **HyperLogLog** algorithm is able to estimate cardinalities of > 10^9 with a typical accuracy of 2%, using 1.5 kB of memory

!!The cardinality of a multiset of uniformly distributed random numbers can be estimated by calculating the maximum number of leading zeros in the binary representation of each number in the set. 

-->

If the maximum number of leading zeros observed is n, an estimate for the number of distinct elements in the set is 2^n

HLL steps:
1. Add a new element ν to the set  
 ![HLLAddOperation](img/HLLAddOperation.jpg)
<!-- Explain by using the image HLLAddOperation -->

hash function should return uniform dist random numbers with almost SAME cardinality
m = size of Array M
b = log2(m) , the array position to modify
ρ(w) = position of leftmost 1 in leftover bits

2. Count to get the cardinality of the set by computing the Harmonic mean of the m registers

3. Merge to get the union of two sets by obtaining the maximum for each pair of registers

Loads of implementation details , eg how to handle variance , 
diff cases for small cardinalities, how to estimate some constants mostly 

The  algorithm stores many estimators instead of one and averages the results using Harmonic Mean (to cope with certain skew patterns)
Bit Patterns Observables && Stohastic Averaging do the job

Applications:
- Cardinality Estimation in Databases (Redis, Druid)
- Druid estimate dimension cardinalities on the fly across multiple granular buckets
- Detecting Worm Propagation/ Network Attacks



## Slide 3
**LSH (Locality Sensitive Hashing)**
Problem at Hand : How to compare structured or unstructured multidimensional Entities (customers , orders , documents , images) ?

Basic Terms : 
 * K-Shingles : any substring of length k , k should be picked large enough that the probability of any given shingle
        appearing in any given document is low , depending on document type
 * Jaccard Similarity : given sets S and T then |S ∩ T |/|S ∪ T |, the ratio
              of the size of the intersection of S and T to the size of their union.
 * Hash Shingles , map to bucket and thus represent document with bucket number! Each bucket contains similar shingles 
 * Compress document , since N-shingle is mapped to M-bucket M <<< N
 * Still Sets representing shingle signatures of Documents can be huge , replace them with Signatures generated from the Sets!
 * Model Signatures of documents or structured customer info in Characteristic Matrices!
 * Compute MinHash signature on the Matrix and generate the Signature matrix!
  * To minhash a set represented by a column of the characteristic matrix, pick
        a permutation of the rows. The minhash value of any column is the number of
        the first row, in the permuted order, in which the column has a 1.
 * We can estimate (a very good estimate) the Jaccard similarities of the underlying sets from this
    signature matrix


| Element | S1  | S2  | S3  | S4 |
|---|---|---|---|---|
| a   |  1 | 0  |  0 | 1  |
| b  |  0 |  0 |  1 |  0 |
| c  |  0 |  1 |  0 |  1 |
| d  |  1 |  0 |  0 |  1 |
| e  |  0 |  0 |  0 | 0  |

Matrix Representation of a Set


| Row   | S1  | S2  | S3  |  S4 | x+1 mod 5   | 3x+1 mod 5  |
|---|---|---|---|---|---|---|
| 0  | 1  |  0 | 0  | 1  | 1  | 1 |
| 1  | 0  |  0 | 1  | 0  | 2  | 4 |
| 2 |  0 |  1 |  0 |  1 |  3 | 2 |
| 3  | 1  | 0  | 1  | 1  | 4  | 0 |
| 4  | 0  |  0 | 1 |  0 | 0  | 3 |

Matrix permutation using Hash functions 

Compute output using Hash Functions :
1. Compute h1(r), h2(r), . . . , hn(r).
2. For each column c do the following:
(a) If c has 0 in row r, do nothing.
(b) However, if c has 1 in row r, then for each i = 1, 2, . . . , n set SIG(i, c)
to the smaller of the current value of SIG(i, c) and hi(r)

|   |  S1 |  S2 | S3  | S4  |
|---|---|---|---|---|
| h1  | 1  | 3  |  0 | 1  |
| h2  | 0  |  2 |  0 | 0  |


Simple Collaborative Filtering using Jaccard Similarity:
* Similar customer purchages
* Similar movie/content preferences
* either standalone (bit simplistic) or by combining it with clustering and more advanced ML techniques


### LSH

Reduces the dimensionality of high-dimensional data and computes their similarity

General approach: 

1. Hashes input items so that similar items map to the same “buckets” with high probability 
2. Check only _candidate pairs_ : Items that hashed on the same bucket

False positives and False Negatives? Small fraction of the trully Similar pairs

Choose Hashings? 

An Example is to use MinHash Signatures
- Use _MinHash Signatures_ to compress sparse matrices to small signatures
- Divide the signature matrix into b bands of r rows each
- Foreach band there is a hash function that takes the portion of the column in that band and hashes them to some large number of buckets

 ![Signature Matrix Division](img/SignatureMatrixDivision.jpg)
<!-- Insert SignatureMatrixDivision.jpg to explain minhash signatures,bands etc -->

Others is Bit Sampling for Hamming Distance, Nilsimsa Hash, TLSH etc.

## Slide 4

LSH Steps with MinHash:

1. Pick a length n for the minhash signatures. Compute the minhash signatures for all the items.
2. Choose a threshold t that defines how similar items have to be in
order for them to be regarded as a desired “similar pair.” Pick a number
of bands b and a number of rows r such that br = n, and the threshold
t is approximately (1/b) ^ 1/r. If avoidance of false negatives is important,
you may wish to select b and r to produce a threshold lower than t; if
speed is important and you wish to limit false positives, select b and r to
produce a higher threshold.
3. Construct candidate pairs by applying the LSH (band) technique.
4. Examine each candidate pair’s signatures and determine whether the fraction
of components in which they agree is at least t.
5. Optionally, if the signatures are sufficiently similar, go to the documents
themselves and check that they are truly similar, rather than documents
that, by luck, had similar signatures.

Applications:
- Near-duplicate detection
- Hierarchical clustering
- Genome-wide association study
- Image similarity identification (VisualRank)
- Gene expression similarity identification
- Audio similarity identification
- Nearest neighbor search
- Audio fingerprint
- Digital video fingerprinting

## Slide 5
**Bloom-Filter**

A Probabilistic Data Structure to test whether an element is a member of a set.

Characteristics:

* False positives are possible but false negatives are not.
* Elements can be added to the set but not removed.

Definition:

* Empty Bloom Filter is a bit array of m bits all set to 0 (proportional to the elements added)
* k different functions (constant) defined each of which hashes a set element to one of m array positions (Uniform Random Distribution). 
>! Problem if k>> then either use one wide output hash function and slice its output or use different initial values to the hash function for every k needed.

>!! False positive rate is k-dependent . k must be selected wisely to avoid Bloom-Filter regeneration (k=m/n* ln2 where m:number of bits in array, n: number of inserted elements)

Operations:
1. Add an element: Feed the element to the k functions to get k array positions. Set all those bits to 1.
2. Query for an element: Feed it to the k hash functions to get k array positions. If any of the bits at these positions is 0, the element is definitely not in the set.
3. No delete operation 

The more hash functions you have:

* the slower your bloom filter
* the quicker it fills up.  
* If you have too few, you may suffer too many false positives (earlier). 
* Sample functions : murmur, FNV series of hash functions , Jenkins hash. 
* Pass input from hash function then `mod` output over array size and get a number less than or equals to array size.

Properties:

* Fixed size Bloom-Filter can represent an arbitarily large number of elements
* False positives rate increase as the number of input element increases till all bits =1
* Union and Intersection of Bloom Filters with same size and set of functions can be implemented with bitwise AND and OR operations

Applications:

* Cache Filtering (Akamai)
* Reduce the disk lookups for non-existent rows or columns (HBase,BigTable,Cassandra,PostgreSQL)
* Decentralized Aggregation && Data synchronization  (RnD projects)
* Malicious URL identification (Chrome)
* E-Wallet synchronization (BitCoin)
* Log lookup (Ethereum)
* Avoid recommending articles/content a user has previously read
* Spellchecking and password blacklisting in early UNIX versions
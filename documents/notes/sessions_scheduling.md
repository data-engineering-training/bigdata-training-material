


1. Intro && Architecture -- 12/9/2018
2. Map Reduce Theory,Examples && HDFS -- 19/9/2018
3. MapReduce Recap, Pregel model, Big Data Algos -- 26/9/2018
4. HBase && Data Lake Design -- 3/10/2018
5. Hive && Impala -- 10/10/2018
6. Hive && Impala handson -- 17/10/2018
7. Spark I -- 24/10/2018
8. Spark II and hands on -- 31/10/2018 
9.  Yarn && Spark hands on -- 7/11/2018
10. Oozie && Airlfow && Integration Patterns && Handson -- 14/11/2018
11. Recap -- 21/11/2018 
12. Hackathon -- 24/11/2018

cluster name : bigdatatraining

cluster admin : admin
cluster passwd : b1Gd@tum3ng

ssh username : sshuser 
ssh passwd : b1Gd@tum3ng

pnas storage account : 
name : pnasstorage

key1:
key : daFrZC2wNDzPK/VuuGPpAivMTxRBUWPC4qSwCXsyGQXfsCOC/sgHVrUC6UPyQ63i2WjzHHfi/lEu+brcJNC2gg==
conn string : DefaultEndpointsProtocol=https;AccountName=pnasstorage;AccountKey=daFrZC2wNDzPK/VuuGPpAivMTxRBUWPC4qSwCXsyGQXfsCOC/sgHVrUC6UPyQ63i2WjzHHfi/lEu+brcJNC2gg==;EndpointSuffix=core.windows.net

key2:
key : TrbvXj3WKOJ1DyByGw2eYcAcrKaI/tKMsCfoW9gyoLHHBp3/mRt3XhIzkDCvhacGBFLIJoos9UzsnR01b/Da5A==
conn string : DefaultEndpointsProtocol=https;AccountName=pnasstorage;AccountKey=TrbvXj3WKOJ1DyByGw2eYcAcrKaI/tKMsCfoW9gyoLHHBp3/mRt3XhIzkDCvhacGBFLIJoos9UzsnR01b/Da5A==;EndpointSuffix=core.windows.net

------------------------------------

cluster name : agiletpchive
cluster url : https://agiletpchive.azurehdinsight.net
admin / ag!l3HiV4p@ss

resource group : agiletpchivers
storage account : agilehivestorage
container : agiletpchive-2018-10-10t07-39-40-140z

key1:
key : QdIAR4LTdhOqgbhtRXQVPifqp8YbE/TMWt87tS2lOjbT91tK0f/vDm68kkLGZww14fqF3ci/aX9Ux7p1raDpjA==
connection string : DefaultEndpointsProtocol=https;AccountName=agilehivestorage;AccountKey=QdIAR4LTdhOqgbhtRXQVPifqp8YbE/TMWt87tS2lOjbT91tK0f/vDm68kkLGZww14fqF3ci/aX9Ux7p1raDpjA==;EndpointSuffix=core.windows.net

key2:
key : EjAiYM6aOox5Eo9L0yOjbADINh/0KykmMcuw5rzm92nIkLiWJZsCjBJAooqAABUwkgb71sGZ8W5x+AAe9EggiA==
connection string: DefaultEndpointsProtocol=https;AccountName=agilehivestorage;AccountKey=EjAiYM6aOox5Eo9L0yOjbADINh/0KykmMcuw5rzm92nIkLiWJZsCjBJAooqAABUwkgb71sGZ8W5x+AAe9EggiA==;EndpointSuffix=core.windows.net
package com.agileactors.examples;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.NamespaceDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.io.compress.Compression.Algorithm;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.RegionLocator;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.util.Pair;

public class HBaseDDLOperations {

	private Configuration config;
	private Connection connection;

	public void connect() {
		config = HBaseConfiguration.create();

		config.set("hbase.zookeeper.quorum", "10.9.48.145");
		config.set("hbase.zookeeper.property.clientPort", "2181");
		config.set("hbase.cluster.distributed", "true");
		config.set("zookeeper.znode.parent", "/hbase");

		// create an admin object using the config
		try {
			connection = ConnectionFactory.createConnection(config);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createTable() {

		try {
			Admin admin = connection.getAdmin();

			// create the table...
			HTableDescriptor tableDescriptor = new HTableDescriptor(
					TableName.valueOf("bigdatatrainingpnas", "NYCTaxiTrips"));

			HColumnDescriptor trColumnDescriptor = new HColumnDescriptor("tr");
			trColumnDescriptor.setMaxVersions(6);
			trColumnDescriptor.setMinVersions(2);
			trColumnDescriptor.setTimeToLive(45000);
			trColumnDescriptor.setBlocksize(60000);
//			trColumnDescriptor.setCompressionType(Algorithm.LZO);

			tableDescriptor.addFamily(trColumnDescriptor);
			tableDescriptor.addFamily(new HColumnDescriptor("am"));
			admin.createTable(tableDescriptor);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteTable() {

		try {

			Admin admin = connection.getAdmin();
			// Disable, and then delete the table

			TableName tableName = TableName.valueOf("bigdatatrainingpnas", "NYCTaxiTrips");

			admin.disableTable(tableName);
			admin.deleteTable(tableName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void printTableRegions(String tableName) {

		System.out.println(" Printing regions of table: " + tableName);
		TableName tn = TableName.valueOf(tableName);
		RegionLocator locator = null;

		try {
			locator = connection.getRegionLocator(tn);

			Pair<byte[][], byte[][]> pair = locator.getStartEndKeys();

			for (int n = 0; n < pair.getFirst().length; n++) {

				byte[] sk = pair.getFirst()[n];
				byte[] ek = pair.getSecond()[n];

				System.out.println("[" + (n + 1) + "]" + " start key: "
						+ (sk.length == 8 ? Bytes.toLong(sk) : Bytes.toStringBinary(sk)) + ", end key: "
						+ (ek.length == 8 ? Bytes.toLong(ek) : Bytes.toStringBinary(ek)));
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {
			try {
				if (locator != null)
					locator.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void createNameSpace() {

		try {
			Admin admin = connection.getAdmin();

			NamespaceDescriptor.Builder builder = NamespaceDescriptor.create("bigdatatrainingpnas");
			NamespaceDescriptor desc = builder.build();

			admin.createNamespace(desc);

			System.out.println(" Namespace: " + desc);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void disconnect() {
		// create an admin object using the config
		try {
			
			if (connection != null && !connection.isClosed())
				connection.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

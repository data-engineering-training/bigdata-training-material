package com.agileactors.examples;

public class OperationsRunner {

	public static void main(String[] args) {
		HBaseDDLOperations hbaseDDL = new HBaseDDLOperations();
		
		hbaseDDL.connect();
		
		hbaseDDL.createNameSpace();
		
		hbaseDDL.createTable();
		
		hbaseDDL.disconnect();

	}

}

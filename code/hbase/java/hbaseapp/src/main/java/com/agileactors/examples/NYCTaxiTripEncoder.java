package com.agileactors.examples;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Random;

import org.apache.hadoop.hbase.util.Bytes;

public class NYCTaxiTripEncoder {

	Random rnd = new Random(42);
	
	public byte[] vendorDateRandomRowKey(NYCTaxiTrip nycTaxiTrip) {
				
// StringBuffer keyBuf = new StringBuffer(50);
		byte[] rndBytes = new byte[3];
		rnd.nextBytes(rndBytes);

		// keyBuf.append(nycTaxiTrip.getVendorID());
		// keyBuf.append("#");
		// keyBuf.append(nycTaxiTrip.getPULocationID());
		// keyBuf.append("#");
		// keyBuf.append(nycTaxiTrip.getTpep_pickup_datetime().getTime());
		// keyBuf.append("#");
		// keyBuf.append(new String(rndBytes));

		return Bytes.toBytes(
				new String(
						ByteBuffer.allocate(30)
						.putInt(nycTaxiTrip.getVendorID())
						.putChar('#')
						.putInt(nycTaxiTrip.getPULocationID())
						.putChar('#')
						.putLong(nycTaxiTrip.getTpep_pickup_datetime().getTime())
						.putChar('#')
						.put(rndBytes)
						.array()));
	}

	public byte[] vendorPaymentTypeRandomRowKey(NYCTaxiTrip nycTaxiTrip) {

		StringBuffer keyBuf = new StringBuffer(50);
		byte[] rndBytes = new byte[3];
		rnd.nextBytes(rndBytes);

		keyBuf.append(nycTaxiTrip.getVendorID());
		keyBuf.append("#");
		keyBuf.append(nycTaxiTrip.getPayment_type());
		keyBuf.append("#");
		keyBuf.append(Long.MAX_VALUE - nycTaxiTrip.getTpep_pickup_datetime().getTime());
		keyBuf.append("#");
		keyBuf.append(new String(rndBytes));

		return Bytes.toBytes(keyBuf.toString());
	}

	public byte[] decimalEncoder() {
		return new byte[1];
	}

}

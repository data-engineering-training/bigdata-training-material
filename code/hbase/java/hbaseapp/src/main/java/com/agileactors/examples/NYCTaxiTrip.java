package com.agileactors.examples;

import java.math.BigDecimal;
import java.util.Date;

public class NYCTaxiTrip {

	private int VendorID;
	private Date tpep_pickup_datetime;
	private Date tpep_dropoff_datetime;
	private int passenger_count;
	private double trip_distance;
	private int RatecodeID;
	private char store_and_fwd_flag;
	private int PULocationID;
	private int DOLocationID;
	private int payment_type;
	private BigDecimal fare_amount;
	private BigDecimal extra;
	private BigDecimal mta_tax;
	private BigDecimal tip_amount;
	private BigDecimal tolls_amount;
	private BigDecimal improvement_surcharge;
	private BigDecimal total_amount;
	
	public NYCTaxiTrip() {}
	
	public NYCTaxiTrip(int vendorID, Date tpep_pickup_datetime, Date tpep_dropoff_datetime, int passenger_count,
			double trip_distance, int ratecodeID, char store_and_fwd_flag, int pULocationID, int dOLocationID,
			int payment_type, BigDecimal fare_amount, BigDecimal extra, BigDecimal mta_tax, BigDecimal tip_amount,
			BigDecimal tolls_amount, BigDecimal improvement_surcharge, BigDecimal total_amount) {
		super();
		VendorID = vendorID;
		this.tpep_pickup_datetime = tpep_pickup_datetime;
		this.tpep_dropoff_datetime = tpep_dropoff_datetime;
		this.passenger_count = passenger_count;
		this.trip_distance = trip_distance;
		RatecodeID = ratecodeID;
		this.store_and_fwd_flag = store_and_fwd_flag;
		PULocationID = pULocationID;
		DOLocationID = dOLocationID;
		this.payment_type = payment_type;
		this.fare_amount = fare_amount;
		this.extra = extra;
		this.mta_tax = mta_tax;
		this.tip_amount = tip_amount;
		this.tolls_amount = tolls_amount;
		this.improvement_surcharge = improvement_surcharge;
		this.total_amount = total_amount;
	}
	
	public int getVendorID() {
		return VendorID;
	}

	public void setVendorID(int vendorID) {
		VendorID = vendorID;
	}

	public Date getTpep_pickup_datetime() {
		return tpep_pickup_datetime;
	}

	public void setTpep_pickup_datetime(Date tpep_pickup_datetime) {
		this.tpep_pickup_datetime = tpep_pickup_datetime;
	}

	public Date getTpep_dropoff_datetime() {
		return tpep_dropoff_datetime;
	}

	public void setTpep_dropoff_datetime(Date tpep_dropoff_datetime) {
		this.tpep_dropoff_datetime = tpep_dropoff_datetime;
	}

	public int getPassenger_count() {
		return passenger_count;
	}

	public void setPassenger_count(int passenger_count) {
		this.passenger_count = passenger_count;
	}

	public double getTrip_distance() {
		return trip_distance;
	}

	public void setTrip_distance(double trip_distance) {
		this.trip_distance = trip_distance;
	}

	public int getRatecodeID() {
		return RatecodeID;
	}

	public void setRatecodeID(int ratecodeID) {
		RatecodeID = ratecodeID;
	}

	public char getStore_and_fwd_flag() {
		return store_and_fwd_flag;
	}

	public void setStore_and_fwd_flag(char store_and_fwd_flag) {
		this.store_and_fwd_flag = store_and_fwd_flag;
	}

	public int getPULocationID() {
		return PULocationID;
	}

	public void setPULocationID(int pULocationID) {
		PULocationID = pULocationID;
	}

	public int getDOLocationID() {
		return DOLocationID;
	}

	public void setDOLocationID(int dOLocationID) {
		DOLocationID = dOLocationID;
	}

	public int getPayment_type() {
		return payment_type;
	}

	public void setPayment_type(int payment_type) {
		this.payment_type = payment_type;
	}

	public BigDecimal getFare_amount() {
		return fare_amount;
	}

	public void setFare_amount(BigDecimal fare_amount) {
		this.fare_amount = fare_amount;
	}

	public BigDecimal getExtra() {
		return extra;
	}

	public void setExtra(BigDecimal extra) {
		this.extra = extra;
	}

	public BigDecimal getMta_tax() {
		return mta_tax;
	}

	public void setMta_tax(BigDecimal mta_tax) {
		this.mta_tax = mta_tax;
	}

	public BigDecimal getTip_amount() {
		return tip_amount;
	}

	public void setTip_amount(BigDecimal tip_amount) {
		this.tip_amount = tip_amount;
	}

	public BigDecimal getTolls_amount() {
		return tolls_amount;
	}

	public void setTolls_amount(BigDecimal tolls_amount) {
		this.tolls_amount = tolls_amount;
	}

	public BigDecimal getImprovement_surcharge() {
		return improvement_surcharge;
	}

	public void setImprovement_surcharge(BigDecimal improvement_surcharge) {
		this.improvement_surcharge = improvement_surcharge;
	}

	public BigDecimal getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(BigDecimal total_amount) {
		this.total_amount = total_amount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + DOLocationID;
		result = prime * result + PULocationID;
		result = prime * result + VendorID;
		result = prime * result + ((tpep_dropoff_datetime == null) ? 0 : tpep_dropoff_datetime.hashCode());
		result = prime * result + ((tpep_pickup_datetime == null) ? 0 : tpep_pickup_datetime.hashCode());
		long temp;
		temp = Double.doubleToLongBits(trip_distance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NYCTaxiTrip other = (NYCTaxiTrip) obj;
		if (DOLocationID != other.DOLocationID)
			return false;
		if (PULocationID != other.PULocationID)
			return false;
		if (VendorID != other.VendorID)
			return false;
		if (tpep_dropoff_datetime == null) {
			if (other.tpep_dropoff_datetime != null)
				return false;
		} else if (!tpep_dropoff_datetime.equals(other.tpep_dropoff_datetime))
			return false;
		if (tpep_pickup_datetime == null) {
			if (other.tpep_pickup_datetime != null)
				return false;
		} else if (!tpep_pickup_datetime.equals(other.tpep_pickup_datetime))
			return false;
		if (Double.doubleToLongBits(trip_distance) != Double.doubleToLongBits(other.trip_distance))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NYCTaxiTrip [VendorID=" + VendorID + ", tpep_pickup_datetime=" + tpep_pickup_datetime
				+ ", tpep_dropoff_datetime=" + tpep_dropoff_datetime + ", passenger_count=" + passenger_count
				+ ", trip_distance=" + trip_distance + ", RatecodeID=" + RatecodeID + ", store_and_fwd_flag="
				+ store_and_fwd_flag + ", PULocationID=" + PULocationID + ", DOLocationID=" + DOLocationID
				+ ", payment_type=" + payment_type + ", fare_amount=" + fare_amount + ", extra=" + extra + ", mta_tax="
				+ mta_tax + ", tip_amount=" + tip_amount + ", tolls_amount=" + tolls_amount + ", improvement_surcharge="
				+ improvement_surcharge + ", total_amount=" + total_amount + "]";
	}
	
	
}

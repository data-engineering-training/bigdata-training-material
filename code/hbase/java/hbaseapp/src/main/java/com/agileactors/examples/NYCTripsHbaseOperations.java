package com.agileactors.examples;

import java.util.List;

public class NYCTripsHbaseOperations {

	public static void main(String[] args) {
		HBaseDMLOperations hbaseOps = new HBaseDMLOperations();
		
		hbaseOps.connect();
		
		NYCTripCsvParser ntcp = new NYCTripCsvParser();
		
		List<NYCTaxiTrip> tripsList = ntcp.processInputFile(args[0]);
		
		hbaseOps.mutatorMultiPut(tripsList);
		
		hbaseOps.disconnect();

	}

}

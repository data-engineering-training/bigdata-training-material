package com.agileactors.examples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import com.agileactors.examples.NYCTaxiTrip;

public class NYCTripCsvParser {

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public List<NYCTaxiTrip> processInputFile(String inputFilePath) {

		List<NYCTaxiTrip> inputList = new ArrayList<NYCTaxiTrip>();

		try {

			File inputF = new File(inputFilePath);

			InputStream inputFS = new FileInputStream(inputF);

			BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));

			// skip the header of the csv

			inputList = br.lines().skip(1).filter(z -> !z.isEmpty()).map(mapToItem).limit(100).collect(Collectors.toList());

			br.close();

		} catch (IOException e) {

			System.out.println(e);

		}

		return inputList;

	}

	private Function<String, NYCTaxiTrip> mapToItem = (line) -> {
				
		String[] p = line.split(",");

		NYCTaxiTrip item = new NYCTaxiTrip();
		try {
			
			item.setVendorID(Integer.valueOf(p[0]));
			
			item.setTpep_pickup_datetime(sdf.parse(p[1]));
			item.setTpep_dropoff_datetime(sdf.parse(p[2]));
			
			item.setPassenger_count(Integer.valueOf(p[3]));
			item.setTrip_distance(Double.valueOf(p[4]));
			item.setRatecodeID(Integer.valueOf(p[5]));
			item.setStore_and_fwd_flag(p[6].charAt(0));
			
			item.setPULocationID(Integer.valueOf(p[7]));
			item.setDOLocationID(Integer.valueOf(p[8]));
			item.setPayment_type(Integer.valueOf(p[9]));
			
			item.setFare_amount(new BigDecimal(p[10]));
			item.setExtra(new BigDecimal(p[11]));
			item.setMta_tax(new BigDecimal(p[12]));
			item.setTip_amount(new BigDecimal(p[13]));
			item.setTolls_amount(new BigDecimal(p[14]));
			item.setImprovement_surcharge(new BigDecimal(p[15]));
			item.setTotal_amount(new BigDecimal(p[16]));
			
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return item;

	};
	
	public static void main(String[] args) {
		//C:\\Users\\Pantelis Nasikas\\Downloads\\nyc_taxi_data\\trips_csv\\yellow_tripdata_2017-01.csv
		NYCTripCsvParser ntcp = new NYCTripCsvParser();
			
		String inputFilePath = args[0];
		
		List<NYCTaxiTrip> theTrips = ntcp.processInputFile(inputFilePath);
		
		theTrips.forEach(z -> System.out.println(z));
				
	}
}

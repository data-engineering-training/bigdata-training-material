package com.agileactors.examples;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.BufferedMutator;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.RetriesExhaustedWithDetailsException;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;


public class HBaseDMLOperations {

	private Configuration config;
	private Connection connection;
	private NYCTaxiTripEncoder nyxTaxiTripEnc = new NYCTaxiTripEncoder();

	public void connect() {
		config = HBaseConfiguration.create();

		config.set("hbase.zookeeper.quorum", "quickstart.cloudera");
		config.set("hbase.zookeeper.property.clientPort", "2181");
		config.set("hbase.cluster.distributed", "true");
		config.set("zookeeper.znode.parent", "/hbase");

		// create an admin object using the config
		try {
			connection = ConnectionFactory.createConnection(config);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void simplePut(NYCTaxiTrip nycTaxiTrip) {

		Table table = null;

		try {

			table = connection.getTable(TableName.valueOf("bigdatatraining", "NYCTaxiTrips"));

			Put put = new Put(nyxTaxiTripEnc.vendorDateRandomRowKey(nycTaxiTrip));

			put = mapFieldsToCFs(nycTaxiTrip, put);

			table.put(put);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			try {
				if (table != null)
					table.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private Put mapFieldsToCFs(NYCTaxiTrip nycTaxiTrip, Put put) {

		put.addColumn(Bytes.toBytes("tr"), Bytes.toBytes("rc"), Bytes.toBytes(nycTaxiTrip.getRatecodeID()));
		put.addColumn(Bytes.toBytes("tr"), Bytes.toBytes("dod"),
				Bytes.toBytes(nycTaxiTrip.getTpep_dropoff_datetime().getTime()));
		put.addColumn(Bytes.toBytes("tr"), Bytes.toBytes("dol"), Bytes.toBytes(nycTaxiTrip.getDOLocationID()));
		put.addColumn(Bytes.toBytes("tr"), Bytes.toBytes("pc"), Bytes.toBytes(nycTaxiTrip.getPassenger_count()));
		put.addColumn(Bytes.toBytes("tr"), Bytes.toBytes("pt"), Bytes.toBytes(nycTaxiTrip.getPayment_type()));
		put.addColumn(Bytes.toBytes("tr"), Bytes.toBytes("sf"), Bytes.toBytes(nycTaxiTrip.getStore_and_fwd_flag()));

		put.addColumn(Bytes.toBytes("am"), Bytes.toBytes("ex"), Bytes.toBytes(nycTaxiTrip.getExtra()));
		put.addColumn(Bytes.toBytes("am"), Bytes.toBytes("fa"), Bytes.toBytes(nycTaxiTrip.getFare_amount()));
		put.addColumn(Bytes.toBytes("am"), Bytes.toBytes("is"), Bytes.toBytes(nycTaxiTrip.getImprovement_surcharge()));
		put.addColumn(Bytes.toBytes("am"), Bytes.toBytes("mta"), Bytes.toBytes(nycTaxiTrip.getMta_tax()));
		put.addColumn(Bytes.toBytes("am"), Bytes.toBytes("tpa"), Bytes.toBytes(nycTaxiTrip.getTip_amount()));
		put.addColumn(Bytes.toBytes("am"), Bytes.toBytes("toa"), Bytes.toBytes(nycTaxiTrip.getTolls_amount()));
		put.addColumn(Bytes.toBytes("am"), Bytes.toBytes("ta"), Bytes.toBytes(nycTaxiTrip.getTotal_amount()));

		return put;
	}

	public void listMultiPut(List<NYCTaxiTrip> tripsList) {
		Table table = null;

		try {

			TableName name = TableName.valueOf("bigdatatraining", "NYCTaxiTrips");
			table = connection.getTable(name);

			List<Put> puts = new ArrayList<Put>();

			for (NYCTaxiTrip nycTaxiTrip : tripsList) {

				Put put = new Put(nyxTaxiTripEnc.vendorDateRandomRowKey(nycTaxiTrip));

				put = mapFieldsToCFs(nycTaxiTrip, put);

				puts.add(put);

			}

			table.put(puts);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			try {

				if (table != null)
					table.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void mutatorMultiPut(List<NYCTaxiTrip> tripsList) {

		Table table = null;
		BufferedMutator mutator = null;
		int batchSize = 50;
		int counter = 0;

		try {
			TableName name = TableName.valueOf("bigdatatraining", "NYCTaxiTrips");
			table = connection.getTable(name);

			mutator = connection.getBufferedMutator(name);
			for (NYCTaxiTrip nycTaxiTrip : tripsList) {

				Put put = new Put(nyxTaxiTripEnc.vendorDateRandomRowKey(nycTaxiTrip));

				put = mapFieldsToCFs(nycTaxiTrip, put);

				mutator.mutate(put);

				if (counter % batchSize == 0) {
					mutator.flush();
					counter = 0;
				} else
					counter++;

			}

			mutator.flush();

		} catch (RetriesExhaustedWithDetailsException e) {

			int numErrors = e.getNumExceptions();
			System.out.println(" Number of exceptions: " + numErrors);
			for (int n = 0; n < numErrors; n++) {
				System.out.println(" Cause[" + n + "]: " + e.getCause(n));
				System.out.println(" Hostname[" + n + "]: " + e.getHostnamePort(n));
				System.out.println(" Row[" + n + "]: " + e.getRow(n));

			}

			System.out.println(" Cluster issues: " + e.mayHaveClusterIssues());
			System.out.println(" Description: " + e.getExhaustiveDescription());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			try {
				if (mutator != null)
					mutator.close();

				if (table != null)
					table.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public void getSimple(NYCTaxiTrip nycTaxiTrip) {

		Table table = null;

		try {
			table = connection.getTable(TableName.valueOf("bigdatatraining", "NYCTaxiTrips"));

			Get get1 = new Get(nyxTaxiTripEnc.vendorDateRandomRowKey(nycTaxiTrip));

			get1.addColumn(Bytes.toBytes("colfam1"), Bytes.toBytes("qual1"));

			Result result1 = table.get(get1);
			byte[] val = result1.getValue(Bytes.toBytes("colfam1"), Bytes.toBytes(" qual1"));
			System.out.println(" Value: " + Bytes.toString(val));

			Get get2 = new Get(Bytes.toBytes("row1")).setId(" GetFluentExample").setMaxVersions().setTimeStamp(1)
					.addColumn(Bytes.toBytes("colfam1"), Bytes.toBytes("qual1")).addFamily(Bytes.toBytes("colfam2"));

			Result result2 = table.get(get2);

			System.out.println(" Result: " + result2);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {

			try {

				if (table != null)
					table.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public void listOfGets() {

		Table table = null;

		try {
			table = connection.getTable(TableName.valueOf("testtable"));

			byte[] cf1 = Bytes.toBytes("colfam1");
			byte[] qf1 = Bytes.toBytes("qual1");
			byte[] qf2 = Bytes.toBytes("qual2");
			byte[] row1 = Bytes.toBytes("row1");
			byte[] row2 = Bytes.toBytes("row2");

			List<Get> gets = new ArrayList<Get>();

			Get get1 = new Get(row1);
			get1.addColumn(cf1, qf1);
			gets.add(get1);
			Get get2 = new Get(row2);
			get2.addColumn(cf1, qf1);
			gets.add(get2);
			Get get3 = new Get(row2);
			get3.addColumn(cf1, qf2);
			gets.add(get3);
			Result[] results = table.get(gets);

			for (Result result : results) {
				String row = Bytes.toString(result.getRow());
				System.out.print(" Row: " + row + " ");
				byte[] val = null;
				if (result.containsColumn(cf1, qf1)) {
					val = result.getValue(cf1, qf1);
					System.out.println(" Value: " + Bytes.toString(val));
				}
				if (result.containsColumn(cf1, qf2)) {
					val = result.getValue(cf1, qf2);
					System.out.println(" Value: " + Bytes.toString(val));
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {

			try {

				if (table != null)
					table.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public List<NYCTaxiTrip> simpleScanner(NYCTaxiTrip nycTaxiTrip1 , NYCTaxiTrip nycTaxiTrip2) {
		
		Table table = null;
		List<NYCTaxiTrip> scanResList = new ArrayList(0);
		
		try {
			table = connection.getTable(TableName.valueOf("bigdatatraining", "NYCTaxiTrips"));
			
			Scan scan4 = new Scan();
			scan4.addFamily(Bytes.toBytes("am"))
				.setBatch(100)
				.setMaxResultSize(500)
				.setMaxVersions(2)
				.setTimeRange(191912288, 192912288)
				.setStartRow(nyxTaxiTripEnc.vendorDateRandomRowKey(nycTaxiTrip1))
				.setStopRow(nyxTaxiTripEnc.vendorDateRandomRowKey(nycTaxiTrip2));

			ResultScanner scanner4 = table.getScanner(scan4);

			for (Result res : scanner4) {
				NYCTaxiTrip resultTrip = new NYCTaxiTrip();
				
				String[] theRowKey = new String( res.getRow() ).split("#") ;
				
				resultTrip.setVendorID(Integer.valueOf(theRowKey[0]));
				resultTrip.setPULocationID(Integer.valueOf(theRowKey[1]));
				resultTrip.setTpep_pickup_datetime(new Date((Long.valueOf(theRowKey[2])) ));
				
				resultTrip.setExtra(Bytes.toBigDecimal( res.getValue(Bytes.toBytes("am"), Bytes.toBytes("ex")) ));
				resultTrip.setFare_amount(Bytes.toBigDecimal( res.getValue(Bytes.toBytes("am"), Bytes.toBytes("fa")) ));
				resultTrip.setImprovement_surcharge(Bytes.toBigDecimal( res.getValue(Bytes.toBytes("am"), Bytes.toBytes("is")) ));
				resultTrip.setMta_tax(Bytes.toBigDecimal(res.getValue(Bytes.toBytes("am"), Bytes.toBytes("mta")) ));
				resultTrip.setTip_amount(Bytes.toBigDecimal(res.getValue(Bytes.toBytes("am"), Bytes.toBytes("tpa")) ));
				resultTrip.setTolls_amount(Bytes.toBigDecimal(res.getValue(Bytes.toBytes("am"), Bytes.toBytes("toa")) ));
				resultTrip.setTotal_amount(Bytes.toBigDecimal(res.getValue(Bytes.toBytes("am"), Bytes.toBytes("ta")) ));
				
				scanResList.add(resultTrip);
			}

			scanner4.close();

			return scanResList;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {

			try {

				if (table != null)
					table.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return scanResList;

	}

	public void disconnect() {
		try {
			
			if (connection != null && !connection.isClosed())
				connection = ConnectionFactory.createConnection(config);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
}

import happybase
import csv
import io
import struct
from datetime import datetime

hbase_thrift = '10.9.48.145' #'quickstart.cloudera'
nyc_taxi_trips_table = 'bigdatatraining:NYCTaxiTrips2'
nyc_taxi_trips_column_families = {'tr': dict(max_versions=6, in_memory = True, time_to_live = 45000),
                                    'am': dict(max_versions=1, block_cache_enabled=False)
                                    }
## or sys.maxsize
max_long = 9223372036854775807

class NYTaxiTrip(object):

    def __init__(self, VendorID,tpep_pickup_datetime,tpep_dropoff_datetime,passenger_count,trip_distance,
                        RatecodeID,store_and_fwd_flag,PULocationID,DOLocationID,payment_type,fare_amount,extra,
                        mta_tax,tip_amount,tolls_amount,improvement_surcharge,total_amount ):

        self.VendorID = VendorID
        self.tpep_pickup_datetime = tpep_pickup_datetime
        self.tpep_dropoff_datetime = tpep_dropoff_datetime
        self.passenger_count = passenger_count
        self.trip_distance = trip_distance
        self.RatecodeID = RatecodeID 
        self.store_and_fwd_flag = store_and_fwd_flag
        self.PULocationID = PULocationID
        self.DOLocationID = DOLocationID
        self.payment_type = payment_type
        self.fare_amount = fare_amount
        self.extra = extra
        self.mta_tax = mta_tax
        self.tip_amount = tip_amount
        self.tolls_amount = tolls_amount
        self.improvement_surcharge = improvement_surcharge
        self.total_amount = total_amount

def vendor_date_random_row_key(taxi_trip) :
    key = io.StringIO()
    key.write( '{:d}'.format(taxi_trip.VendorID) )
    key.write("#")
    key.write('{:>3d}'.format(taxi_trip.PULocationID))
    key.write("#")
    key.write( '{:.0f}'.format(datetime.strptime(taxi_trip.tpep_pickup_datetime, '%Y-%m-%d %H:%M:%S').timestamp() ) ) 

    return key.getvalue()

def vendor_payment_type_random_row_key(taxi_trip) :

    key = io.StringIO()
    key.write( '{:d}'.format(taxi_trip.VendorID) )
    key.write("#")
    key.write('{:>3d}'.format(taxi_trip.payment_type))
    key.write("#")
    inverse_ts = max_long - datetime.strptime(taxi_trip.tpep_pickup_datetime, '%Y-%m-%d %H:%M:%S').timestamp()
    key.write( '{:.0f}'.format( inverse_ts ) ) 

    return key.getvalue()

class NYCTaxiHbaseManager(object):

    def __init__(self, table_name , connection):
        self.table = connection.table(nyc_taxi_trips_table)
    
    def map_trip_to_cf(self, taxi_trip):
    
        trip_dict = {}

        trip_dict[b'tr:rc'] =  bytes(taxi_trip.RatecodeID, 'utf-8')
        trip_dict[b'tr:dod'] = bytes( bytearray(struct.pack("f",datetime.strptime(taxi_trip.tpep_dropoff_datetime, '%Y-%m-%d %H:%M:%S').timestamp() )))
        trip_dict[b'tr:dol'] = bytes(taxi_trip.DOLocationID, 'utf-8')
        trip_dict[b'tr:pc']  = bytes(taxi_trip.passenger_count, 'utf-8')
        trip_dict[b'tr:pt']  = bytes(taxi_trip.payment_type, 'utf-8')
        trip_dict[b'tr:sf']  = bytes(taxi_trip.store_and_fwd_flag, 'utf-8')

        trip_dict[b'am:ex']  =  bytes(taxi_trip.extra , 'utf-8')
        trip_dict[b'am:fa']  = bytes(taxi_trip.fare_amount , 'utf-8')
        trip_dict[b'am:is']  =  bytes(taxi_trip.improvement_surcharge , 'utf-8')
        trip_dict[b'am:mta']  = bytes(taxi_trip.mta_tax , 'utf-8')
        trip_dict[b'am:tpa']  = bytes(taxi_trip.tip_amount , 'utf-8')
        trip_dict[b'am:toa']  = bytes(taxi_trip.tolls_amount , 'utf-8')
        trip_dict[b'am:ta']  = bytes(taxi_trip.total_amount , 'utf-8')
		
        return trip_dict

    def put(self, taxi_trip):

        key = vendor_date_random_row_key(taxi_trip)
        column_values = self.map_trip_to_cf(taxi_trip)

        self.table.put(key, column_values)

    def put_batch(self, taxi_trip_list):
        
        try:

            with self.table.batch(batch_size=100) as batch_ops:

                for taxi_trip in taxi_trip_list:
                    key = vendor_date_random_row_key(taxi_trip)
                    column_values = self.map_trip_to_cf(taxi_trip)
                
                    batch_ops.put(key, column_values)

        except ValueError as ve: 
            print(ve)

    def get(self, taxi_trip , columns_ref = None, include_timestamp=True):

        row = self.table.row( vendor_date_random_row_key(taxi_trip), columns=columns_ref)

        return row

    def get_versions(self, taxi_trip , columns_ref = None , cell_versions = 2, cell_include_timestamp=True ):
    
        row_cells = self.table.cells( vendor_date_random_row_key(taxi_trip), columns=columns_ref , 
                                versions = cell_versions , include_timestamp = cell_include_timestamp)

        return row_cells


    def scan(self, start_row , stop_row = None):
        taxi_trips_results = {}
        for key, data in self.table.scan(row_start=start_row ,  row_stop= start_row):
            taxi_trips_results[key] = data

    

class hbase_ops(object):

    def __init__(self, hostname , thrift_port = 9090):
        
        self.connection = happybase.Connection(host=hostname, port=thrift_port, timeout=None, 
                                        autoconnect=True, transport='buffered', protocol='binary')

    def close(self):

        if self.connection is not None:
            self.connection.close()

    def create_table(self , tablename , column_families_dict):
        self.connection.create_table(tablename, column_families_dict)

    def delete_table(self, tablename):
        self.connection.disable_table(tablename)
        self.connection.delete_table(tablename)

    def list_tables(self):
        # returns table names list
        hbase_tables = self.connection.tables()

        return hbase_tables
    
def read_sample_file(filename , num_rows = 1000):
    sample_rows = []
    with open(filename, newline='\n') as csvfile:

        tripsreader = csv.reader(csvfile, delimiter=',')
        
        next(tripsreader)

        for row in tripsreader:
            
            if len(row) == 17:
                the_trip = NYTaxiTrip(int(row[0]),row[1],row[2],row[3],row[4],row[5],row[6],int(row[7]),row[8],row[9],
                                    row[10],row[11],row[12],row[13],row[14],row[15],row[16])

                sample_rows.append(the_trip)
                

            if len(sample_rows) == num_rows:
                break

    return sample_rows

if __name__ == '__main__':

    theops = hbase_ops(hbase_thrift)
    theops.create_table(nyc_taxi_trips_table , nyc_taxi_trips_column_families)

    print(theops.list_tables())

    # taxi_manager = NYCTaxiHbaseManager(nyc_taxi_trips_table , theops.connection)

    # sample_trips_list = read_sample_file('C:\\Users\\Pantelis Nasikas\\Downloads\\nyc_taxi_data\\trips_csv\\yellow_tripdata_2017-01.csv')

    # taxi_manager.put_batch(sample_trips_list)


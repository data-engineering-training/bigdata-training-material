## Create a new namespace
create_namespace 'bigdatacourse'

## Create table and two column families cf1 , cf2
create 'bigdatacourse:testtable' , 'cf1' , 'cf2' 

## Create table and two column families cf1 , cf2 , 
## column family cf1 keeps 5 versions of each entry , cf2 the default ,
## table is split in 5 regions as data are ingested
create 'bigdatacourse:testtable2',   {NAME => 'cf1', VERSIONS => 5} , { NAME => 'cf2' } , {NUMREGIONS => 5, SPLITALGO => 'HexStringSplit'}

## Create table with one column family , set ttl (value in seconds) for entries , try to keep it in the blockcache
create 'bigdatacourse:bctable1', {NAME => 'f1', VERSIONS => 1, TTL => 36000, BLOCKCACHE => true}

## Describe a defined table
describe 'bigdatacourse:testtable2'

## Grant authorizations to tables to groups and users
grant '@admins', 'RWXCA' , 'bigdatacourse:testtable2'

grant 'webuser', 'R' , 'bigdatacourse:testtable2'

## Put data in a table one cell at a time 
put 'bigdatacourse:testtable2', 'AAA11111', 'cf1:data', 'value for column'

## scan table for data 
scan 'bigdatacourse:testtable2'

## scan output
ROW                                                       COLUMN+CELL
 AAA11111                                                 column=cf1:data, timestamp=1537893718720, value=value for column
1 row(s) in 0.1000 seconds

## Scan Range , select column families , set row key ranges or timestamp ranges

scan 'bigdatacourse:testtable2', {COLUMNS => ['cf1', 'cf2'], LIMIT => 10, STARTROW => 'wyz' , STOPROW => 'xyz'}
scan 'bigdatacourse:testtable2', {COLUMNS => 'cf1', TIMERANGE => [1303668804, 1303668904]}


## Get needs the rowkey , accepts timestampe insertion range as well
get 'bigdatacourse:testtable2' , 'AAA11111'

## Get a row and project columns , request certain versions or insertion timestamps
get 'bigdatacourse:testtable2' , 'AAA11111', {COLUMN => ['cf1', 'cf2']}

get 'bigdatacourse:testtable2' , 'AAA11111', {COLUMN => 'cf1', TIMESTAMP => ts1}

get 'bigdatacourse:testtable2' , 'AAA11111', {COLUMN => 'cf1', TIMERANGE => [ts1, ts2], VERSIONS => 3}

## Use hbase shell's java libraries if needed 
## eg convert dates to timestamps (to use in Get command)
hbase(main):016:0> import java.text.SimpleDateFormat
=> Java::JavaText::SimpleDateFormat
hbase(main):017:0> import java.text.ParsePosition
=> Java::JavaText::ParsePosition
hbase(main):018:0> SimpleDateFormat.new("yy/MM/dd HH:mm:ss").parse("08/08/16 20:56:29", ParsePosition.new(0)).getTime()
=> 1218920189000

## or timestamps to dates (after results are returned)
hbase(main):021:0> import java.util.Date
hbase(main):022:0> Date.new(1218920189000).toString() => "Sat Aug 16 20:56:29 UTC 2008"
package org.agileactors.bigdata.training.spark.rdd;

// STEP-0: import required classes and interfaces
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
//
import org.apache.spark.Partitioner;
import org.apache.spark.api.java.function.Function2;
import scala.Tuple2;
//
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;



/**
 * This class provides a basic implementation of "left outer join"  
 * operation for a given two tables.  This class is provided as  
 * an educational tool to understand the concept of "left outer join" 
 * functionality.  
 *
 *
 * Note that Spark API does provide JavaPairRDD.leftOuterJoin() functionality.
 *
 * @author Mahmoud Parsian
 *
 */ 
public class SparkLeftOuterJoin {

  public static void main(String[] args) throws Exception {

    String usersInputFile =  "c:\\Users\\Pantelis Nasikas\\Documents\\tpcds_1\\customer.dat"; //args[0];
    String transactionsInputFile = "c:\\Users\\Pantelis Nasikas\\Documents\\tpcds_1\\store_sales.dat";  //args[1];
    String locationsInputFile = "c:\\Users\\Pantelis Nasikas\\Documents\\tpcds_1\\customer_address.dat";

    System.out.println("users="+ usersInputFile);
    System.out.println("transactions="+ transactionsInputFile);
    System.out.println("locations="+ locationsInputFile);
       
    JavaSparkContext ctx = new JavaSparkContext();
					
    JavaRDD<String> users = ctx.textFile(usersInputFile, 1);

    JavaPairRDD<String,String> usersRDD =
          users.mapToPair((String s) -> {
              String[] userRecord = s.split("\\|");

              return new Tuple2<String,String>(userRecord[4] , userRecord[0]);
          } 
    );

    JavaRDD<String> locations = ctx.textFile(locationsInputFile, 1);

    JavaPairRDD<String,String> locationsRDD =
              locations.mapToPair((String s) -> {
                          String[] locationRecord = s.split("\\|");
                          String city = "Default";

                          if (locationRecord.length > 7) {
                              city = locationRecord[6];
                          }

                          return new Tuple2<String,String>(locationRecord[0], city);
                      }
              );

    JavaPairRDD<String,Tuple2<String,String>> usersLocationsRDD =

            usersRDD.join(locationsRDD)
                    .mapToPair( ul ->
                                     new Tuple2<String, Tuple2<String,String>>(
                                        ul._2()._1(), new Tuple2<>("L",ul._2()._2())) );

    JavaRDD<String> transactions = ctx.textFile(transactionsInputFile, 1);

	JavaPairRDD<String,Tuple2<String,String>> transactionsRDD =
          transactions.mapToPair((String s) -> {
              String[] transactionRecord = s.split("\\|");
              Tuple2<String,String> product = new Tuple2<String,String>("P", transactionRecord[2]);
              return new Tuple2<String,Tuple2<String,String>>(transactionRecord[3], product);
          }
    );
    
    JavaPairRDD<String,Tuple2<String,String>> allRDD = transactionsRDD.union(usersLocationsRDD);

    JavaPairRDD<String, Iterable<Tuple2<String,String>>> groupedRDD = allRDD.groupByKey();

    JavaPairRDD<String,String> productLocationsRDD = 
         groupedRDD.flatMapToPair((Tuple2<String, Iterable<Tuple2<String,String>>> s) -> {

             Iterable<Tuple2<String,String>> pairs = s._2;
             String location = "UNKNOWN";
             List<String> products = new ArrayList<String>();
             for (Tuple2<String,String> t2 : pairs) {
                 if (t2._1.equals("L")) {
                     location = t2._2;
                 }
                 else {
                     // t2._1.equals("P")
                     products.add(t2._2);
                 }
             }
             
             // now emit (K, V) pairs
             List<Tuple2<String,String>> kvList = new ArrayList<Tuple2<String,String>>();
             for (String product : products) {
                 kvList.add(new Tuple2<String, String>(product, location));
             }
             // Note that edges must be reciprocal, that is every
             // {source, destination} edge must have a corresponding {destination, source}.
             return kvList.iterator();
        }
    );

    // Find all locations for a product
	JavaPairRDD<String, Iterable<String>> productByLocations = productLocationsRDD.groupByKey();
	productByLocations.saveAsTextFile("C:\\temp\\productByLocation.csv");

    JavaPairRDD<String, Tuple2<Set<String>, Integer>> productByUniqueLocations = 
          productByLocations.mapValues((Iterable<String> s) -> {
              Set<String> uniqueLocations = new HashSet<String>();
              for (String location : s) {
                  uniqueLocations.add(location);
              }
              return new Tuple2<Set<String>, Integer>(uniqueLocations, uniqueLocations.size());
          } 
    );

    productByUniqueLocations.saveAsTextFile("C:\\temp\\productByUniqueLocations.csv");


    System.exit(0);
  }
}

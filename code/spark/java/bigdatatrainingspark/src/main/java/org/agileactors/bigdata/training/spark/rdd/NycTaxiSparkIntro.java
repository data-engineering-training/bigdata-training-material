    package org.agileactors.bigdata.training.spark.rdd;

import org.apache.spark.api.java.*;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.SparkConf;
import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;
import scala.Tuple6;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


//VendorID,tpep_pickup_datetime,tpep_dropoff_datetime,passenger_count,trip_distance,RatecodeID,store_and_fwd_flag,
//        PULocationID,DOLocationID,payment_type,fare_amount,extra,mta_tax,tip_amount,tolls_amount,improvement_surcharge,total_amount
//        1,2017-01-09 11:13:28,2017-01-09 11:25:45,1,3.30,1,N,263,161,1,12.5,0,0.5,2,0,0.3,15.3
//        1,2017-01-09 11:32:27,2017-01-09 11:36:01,1,.90,1,N,186,234,1,5,0,0.5,1.45,0,0.3,7.25
//        1,2017-01-09 11:38:20,2017-01-09 11:42:05,1,1.10,1,N,164,161,1,5.5,0,0.5,1,0,0.3,7.3
//        1,2017-01-09 11:52:13,2017-01-09 11:57:36,1,1.10,1,N,236,75,1,6,0,0.5,1.7,0,0.3,8.5

public class NycTaxiSparkIntro {

    public static void main(String[] args) {

        String appName = "NycTaxiIntro";
        String master = "local[4]";
        String nycTaxiFile = "C:\\Users\\Pantelis Nasikas\\Downloads\\nyc_taxi_data\\trips_csv\\yellow_tripdata_2017-01.csv";

        SparkConf conf = new SparkConf().setAppName(appName).setMaster(master);
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<String> distFile = sc.textFile(nycTaxiFile);

        JavaPairRDD<Integer, Tuple4< Integer, Double, BigDecimal, Integer>> vendorStats =
                distFile.filter(f -> !f.isEmpty() && !f.startsWith("Vend"))
                .map(l -> l.split(","))
                .mapToPair( c -> new Tuple2(Integer.valueOf(c[0]),
                                    new Tuple4(
                                            Integer.valueOf(c[3]),
                                            Double.valueOf(c[4]),
                                            new BigDecimal(c[16]),
                                            new Integer(1))
                                        ));

        Map<Integer, Tuple4<Integer, Double, BigDecimal, Integer>> vendorStatsMap =
                vendorStats.reduceByKey((a, b) -> new Tuple4(a._1() + b._1(),
                                                                a._2() + b._2(),
                                                                a._3().add(b._3()),
                                                                a._4() + b._4()))
                .collectAsMap();

        vendorStatsMap.forEach( (key ,value) -> System.out.println(key + ": " + value));

        sc.stop();
    }

}

package org.agileactors.bigdata.training.spark.df;

import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.expressions.WindowSpec;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.List;
import org.apache.spark.sql.expressions.Window;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.avg;
import static org.apache.spark.sql.functions.sum;

// .\bin\spark-submit.cmd --class "org.agileactors.bigdata.training.spark.df.CustomersCatalogSalesFlow" --master local[4]  "C:\\Users\\Pantelis Nasikas\\IdeaProjects\\bigdatatrainingspark\\target\\bigdatatraining
//        -spark-1.0-SNAPSHOT.jar" --driver-class-path postgresql-9.4.1207.jar
public class CustomersCatalogSalesFlow {

    public static void main(String[] args) {


        SparkSession spark = SparkSession
                .builder()
                .appName("Customers Catalog Sales")
                .getOrCreate();

        StructType customerSchema = createCustomerSchema();

        Dataset<Row> customerCsv = spark.read().format("csv")
                .option("sep", "|")
                .schema(customerSchema)
                .load("C:\\Users\\Pantelis Nasikas\\Documents\\tpcds_1\\customer.dat");

        Dataset<Row> catalogSalesDF = spark.read()
                .format("jdbc")
                .option("url", "jdbc:postgresql://localhost:5432/tpcds1")
                .option("dbtable", "public.catalog_sales")
                .option("user", "postgres")
                .option("password", "pnasikas")
                .load();

        Dataset<Row> dateDimDF = spark.read()
                .format("jdbc")
                .option("url", "jdbc:postgresql://localhost:5432/tpcds1")
                .option("dbtable", "public.date_dim")
                .option("user", "postgres")
                .option("password", "pnasikas")
                .load();


        Dataset<Row> customerSalesDf = catalogSalesDF.alias("cs")
                .join(customerCsv.alias("c"),

                        col("cs.cs_bill_customer_sk").equalTo(col("c.c_customer_sk")),"inner")

                .select(col("c_customer_sk"), col("cs_order_number") ,
                        col("cs_sold_date_sk"), col("cs_quantity") ,
                        col("cs_list_price"), col("cs_wholesale_cost"))
                .distinct();

        customerSalesDf.show(100);

        WindowSpec customerMonthWindowSpec = Window
                                                .partitionBy(col("c_customer_sk"), col("d_month_seq"))
                                                .orderBy(col("d_month_seq").asc());


        Dataset<Row> customerSalesMonthAgg = customerSalesDf.alias("ss")
                        .join(dateDimDF.alias("d"),
                        col("ss.cs_sold_date_sk").equalTo(col("d.d_date_sk")) , "inner")
                        .select(col("c_customer_sk"), col("d.d_moy"),
                                sum(col("cs_list_price")).over(customerMonthWindowSpec).alias("list_price_sum"),
                                avg(col("cs_wholesale_cost")).over(customerMonthWindowSpec).alias("avg_wholesale_cost"));

        customerSalesMonthAgg.show(50);

        customerSalesMonthAgg.write().parquet("C:\\Users\\Pantelis Nasikas\\Documents\\tpcds_1\\parquet\\customerSalesMonthAgg.parquet");

        spark.close();

    }

    private static StructType createCustomerSchema() {
        List<StructField> fields = new ArrayList<>();

        StructField c_customer_sk = DataTypes.createStructField("c_customer_sk", DataTypes.IntegerType, true);
        fields.add(c_customer_sk);

        StructField c_customer_id = DataTypes.createStructField("c_customer_id", DataTypes.StringType, true);
        fields.add(c_customer_id);

        StructField c_current_cdemo_sk = DataTypes.createStructField("c_current_cdemo_sk", DataTypes.IntegerType, true);
        fields.add(c_current_cdemo_sk);

        StructField c_current_hdemo_sk = DataTypes.createStructField("c_current_hdemo_sk", DataTypes.IntegerType, true);
        fields.add(c_current_hdemo_sk);

        StructField c_current_addr_sk = DataTypes.createStructField("c_current_addr_sk", DataTypes.IntegerType, true);
        fields.add(c_current_addr_sk);

        StructField c_first_shipto_date_sk = DataTypes.createStructField("c_first_shipto_date_sk", DataTypes.IntegerType, true);
        fields.add(c_first_shipto_date_sk);

        StructField c_first_sales_date_sk = DataTypes.createStructField("c_first_sales_date_sk", DataTypes.IntegerType, true);
        fields.add(c_first_sales_date_sk);

        StructField c_salutation = DataTypes.createStructField("c_salutation", DataTypes.StringType, true);
        fields.add(c_salutation);

        StructField c_first_name = DataTypes.createStructField("c_first_name", DataTypes.StringType, true);
        fields.add(c_first_name);

        StructField c_last_name = DataTypes.createStructField("c_last_name", DataTypes.StringType, true);
        fields.add(c_last_name );

        StructField c_preferred_cust_flag = DataTypes.createStructField("c_preferred_cust_flag", DataTypes.StringType, true);
        fields.add(c_preferred_cust_flag);

        StructField c_birth_day = DataTypes.createStructField("c_birth_day", DataTypes.IntegerType, true);
        fields.add(c_birth_day);

        StructField c_birth_month = DataTypes.createStructField("c_birth_month", DataTypes.IntegerType, true);
        fields.add(c_birth_month);

        StructField c_birth_year = DataTypes.createStructField("c_birth_year", DataTypes.IntegerType, true);
        fields.add(c_birth_year);

        StructField c_birth_country = DataTypes.createStructField("c_birth_country", DataTypes.StringType, true);
        fields.add(c_birth_country);

        StructField c_login = DataTypes.createStructField("c_login", DataTypes.StringType, true);
        fields.add(c_login);

        StructField c_email_address = DataTypes.createStructField("c_email_address", DataTypes.StringType, true);
        fields.add(c_email_address);

        StructField c_last_review_date = DataTypes.createStructField("c_last_review_date", DataTypes.StringType, true);
        fields.add(c_last_review_date);

        return DataTypes.createStructType(fields);
    }

}

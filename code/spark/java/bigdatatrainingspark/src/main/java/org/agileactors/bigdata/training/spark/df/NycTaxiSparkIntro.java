package org.agileactors.bigdata.training.spark.df;

import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.avg;
import static org.apache.spark.sql.functions.sum;
import static org.apache.spark.sql.functions.max;


public class NycTaxiSparkIntro {

    public static void main(String[] args) {

        SparkSession spark = SparkSession
                .builder()
                .appName("NYC Taxi DFs")
                .getOrCreate();

        Dataset<Row> nycTaxiDFCsv = spark.read().format("csv")
                .option("sep", ",")
                .option("inferSchema", "true")
                .option("header", "true")
                .load("C:\\Users\\Pantelis Nasikas\\Downloads\\nyc_taxi_data\\trips_csv\\yellow_tripdata_2017-01.csv");

        nycTaxiDFCsv.show();

        nycTaxiDFCsv.printSchema();

        nycTaxiDFCsv.cache();

        nycTaxiDFCsv.select(col("VendorID"),col("passenger_count"),
                    col("trip_distance"),col("total_amount"))
                    .groupBy(col("VendorID"))
                    .agg(max(col("passenger_count")),sum(col("trip_distance")),
                            avg(col("total_amount")))
        .show();

        nycTaxiDFCsv.createOrReplaceTempView("nyc_taxi_trips");

        Dataset<Row> sqlDF = spark.sql("SELECT VendorID, max(passenger_count), sum(trip_distance), avg(total_amount) " +
                "FROM nyc_taxi_trips group by VendorID");

        sqlDF.show();
    }

}

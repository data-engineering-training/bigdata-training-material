from pyspark.sql import SparkSession
from pyspark.sql.functions import sum, max, avg, expr


if __name__ == "__main__":

    spark = SparkSession \
        .builder \
        .appName("NYC Taxi Spark Intro") \
        .getOrCreate()

    nyc_trips_file = "C:\\Users\\Pantelis Nasikas\\Downloads\\nyc_taxi_data\\trips_csv\\yellow_tripdata_2017-01.csv"

    nyc_trips_df = spark.read.load(nyc_trips_file,
                         format="csv", sep=",", inferSchema="true", header="true")

    nyc_trips_df.show()

    nyc_trips_df.printSchema()

    nyc_trips_df.cache()

    nyc_trips_df.select("VendorID","passenger_count",  \
                    "trip_distance","total_amount")    \
                    .groupBy("VendorID")   \
                    .agg(max("passenger_count"),sum("trip_distance"),  \
                            avg("total_amount"))  \
                    .show();

    nyc_trips_df.createOrReplaceTempView("nyc_taxi_trips");

    sqlDF = spark.sql("SELECT VendorID, max(passenger_count), sum(trip_distance), avg(total_amount) " +
                                  "FROM nyc_taxi_trips group by VendorID");

    sqlDF.show();
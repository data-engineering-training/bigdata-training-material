from pyspark.sql import SparkSession
from pyspark.sql.functions import sum, max, avg, expr
from pyspark.sql.types import StructType, StructField, StringType , IntegerType
from pyspark.sql.window import Window
from pyspark.sql.functions import asc

if __name__ == "__main__":

    fields = [
              StructField("c_customer_sk", IntegerType(), True), \
              StructField("c_customer_id", StringType(), True), \
              StructField("c_current_cdemo_sk", IntegerType(), True), \
              StructField("c_current_hdemo_sk", IntegerType(), True), \
              StructField("c_current_addr_sk", IntegerType(), True), \
              StructField("c_first_shipto_date_sk", IntegerType(), True), \
              StructField("c_first_sales_date_sk", IntegerType(), True), \
              StructField("c_salutation", StringType(), True), \
              StructField("c_first_name", StringType(), True), \
              StructField("c_last_name", StringType(), True), \
              StructField("c_preferred_cust_flag", StringType(), True), \
              StructField("c_birth_day", IntegerType(), True), \
              StructField("c_birth_month", IntegerType(), True), \
              StructField("c_birth_year", IntegerType(), True), \
              StructField("c_birth_country", StringType(), True), \
              StructField("c_login", StringType(), True), \
              StructField("c_email_address", StringType(), True), \
              StructField("c_last_review_date", StringType(), True), \
              ]

    customer_schema = StructType(fields)

    spark = SparkSession \
        .builder \
        .appName("Customer Catalog Sales Flow") \
        .getOrCreate()

    customer_csv = spark.read.load("C:\\Users\\Pantelis Nasikas\\Documents\\tpcds_1\\customer.dat",
                         format="csv", sep="|", inferSchema="false", header="false", schema= customer_schema)


    catalog_sales_DF = spark.read \
                            .format("jdbc") \
                            .option("url", "jdbc:postgresql://localhost:5432/tpcds1") \
                            .option("dbtable", "public.catalog_sales") \
                            .option("user", "postgres") \
                            .option("password", "pnasikas") \
                            .load()

    date_dim_dF = spark.read \
                        .format("jdbc") \
                        .option("url", "jdbc:postgresql://localhost:5432/tpcds1")  \
                        .option("dbtable", "public.date_dim") \
                        .option("user", "postgres") \
                        .option("password", "pnasikas") \
                        .load()

    customer_sales_Df = catalog_sales_DF.alias("cs") \
                .join(customer_csv.alias("c"), \
                      catalog_sales_DF["cs_bill_customer_sk"] == customer_csv["c_customer_sk"], "inner") \
                .select("c_customer_sk", "cs_order_number", \
                        "cs_sold_date_sk", "cs_quantity", \
                        "cs_list_price", "cs_wholesale_cost") \
                .distinct()

    customer_sales_Df.show(100);


    customer_month_window_spec = Window \
                                    .partitionBy("c_customer_sk", "d_month_seq")  \
                                    .orderBy(asc("d_month_seq"))

    customer_sales_month_Agg = customer_sales_Df.alias("ss") \
                            .join(date_dim_dF.alias("d"), \
                                  customer_sales_Df["cs_sold_date_sk"] == date_dim_dF["d_date_sk"], "inner")  \
                            .select("c_customer_sk", "d.d_moy",  \
                                    sum("cs_list_price").over(customer_month_window_spec) \
                                    .alias("list_price_sum"),  \
                                    avg("cs_wholesale_cost").over(customer_month_window_spec).alias("avg_wholesale_cost"))

    customer_sales_month_Agg.show(50);

    customer_sales_month_Agg.write.parquet(
        "C:\\Users\\Pantelis Nasikas\\Documents\\tpcds_1\\parquet\\customerSalesMonthAgg_python.parquet");

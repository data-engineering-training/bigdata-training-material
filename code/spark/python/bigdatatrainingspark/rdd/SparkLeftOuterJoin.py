from pyspark import SparkContext, SparkConf


users_input_file = "c:\\Users\\Pantelis Nasikas\\Documents\\tpcds_1\\customer.dat"
transactions_input_file = "c:\\Users\\Pantelis Nasikas\\Documents\\tpcds_1\\store_sales.dat"
locations_input_file = "c:\\Users\\Pantelis Nasikas\\Documents\\tpcds_1\\customer_address.dat"


def split_on_char(line):
    return line.split("|")


def map_locations(cells):
    city = "Default"
    if len(cells) > 7:
        city = cells[6]

    return (cells[0], city)


def left_join_mapper(key_values_pair):
    # print(key_values_pair[1])
    value_pairs = list(key_values_pair[1])
    location = 'Location'
    products = []

    for vp in value_pairs:
        if vp[0] == 'L':
            location = vp[1]
        else :
            products.append(vp[1])

    kv_list = []
    for pp in products:
        kv_list.append((pp, location))

    return iter(kv_list)


if __name__ == "__main__":

    spark_conf = SparkConf().setAppName("TPCDS Left Join").setMaster("local[4]")
    spark = SparkContext(conf=spark_conf)

    users_rdd = spark.textFile(users_input_file)
    transactions_rdd = spark.textFile(transactions_input_file)
    locations_rdd = spark.textFile(locations_input_file)

    user_location_sk_rdd = users_rdd.map(split_on_char) \
             .map(lambda cells : (cells[4], cells[0]))

    location_sk_rdd = locations_rdd.map(split_on_char) \
        .map(map_locations)

    user_location_joined_rdd = user_location_sk_rdd.join(location_sk_rdd) \
                        .map(lambda jk : (jk[1][0], ('L', jk[1][1])))

    transactions_products_sk_rdd = transactions_rdd.map(split_on_char) \
        .map(lambda cells: (cells[3], ('P', cells[2])))

    transactions_products_sk_rdd.union(user_location_joined_rdd) \
                                        .groupByKey() \
                                        .flatMap(left_join_mapper) \
                                        .groupByKey() \
                                        .map(lambda x : (x[0], list(x[1]))) \
                                        .saveAsTextFile("c:\\temp\\transactions_products_sk_rdd.csv")

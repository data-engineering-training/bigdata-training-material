from pyspark import SparkContext, SparkConf
from decimal import Decimal

# //VendorID,tpep_pickup_datetime,tpep_dropoff_datetime,passenger_count,trip_distance,RatecodeID,store_and_fwd_flag,
# //        PULocationID,DOLocationID,payment_type,fare_amount,extra,mta_tax,tip_amount,tolls_amount,improvement_surcharge,total_amount
# //        1,2017-01-09 11:13:28,2017-01-09 11:25:45,1,3.30,1,N,263,161,1,12.5,0,0.5,2,0,0.3,15.3
# //        1,2017-01-09 11:32:27,2017-01-09 11:36:01,1,.90,1,N,186,234,1,5,0,0.5,1.45,0,0.3,7.25
# //        1,2017-01-09 11:38:20,2017-01-09 11:42:05,1,1.10,1,N,164,161,1,5.5,0,0.5,1,0,0.3,7.3
# //        1,2017-01-09 11:52:13,2017-01-09 11:57:36,1,1.10,1,N,236,75,1,6,0,0.5,1.7,0,0.3,8.5

def map_columns(columns):
    # lambda columns : ( int(columns[0]), (int(columns[3]), float(columns[4]), Decimal(columns[16]), 1))) \
    # print(columns)
    vId = 0
    passenger_count = 0
    trip_distance = 0
    total_amount = 0.0

    if len(columns[0]) > 0:
        vId = int(columns[0])
    else:
        vId = 0

    if len(columns[3]) > 0:
        passenger_count = int(columns[3])
    else:
        passenger_count = 0

    if len(columns[4]) > 0:
        trip_distance = float(columns[4])
    else:
        trip_distance = None

    return ( vId, (passenger_count, trip_distance, Decimal(columns[16]), 1))


if __name__ == "__main__":

    nyc_trips_file = "C:\\Users\\Pantelis Nasikas\\Downloads\\nyc_taxi_data\\trips_csv\\yellow_tripdata_2017-01.csv"

    spark_conf = SparkConf().setAppName("NycTaxiSparkIntro").setMaster("local[4]")
    spark = SparkContext(conf=spark_conf)

    nyc_trips_rdd = spark.textFile(nyc_trips_file)

    vId_group = nyc_trips_rdd.filter(lambda line : not line.startswith('Vend') and len(line) > 0 ) \
                 .map(lambda line : line.split(",")) \
                 .map(map_columns) \
                 .reduceByKey( lambda v1 , v2 : ( v1[0] + v2[0] ,v1[1] + v2[1] , v1[2] + v2[2] , v1[3] + v2[3] ) ) \
                 .collect()

    for p in vId_group:
        print(p)

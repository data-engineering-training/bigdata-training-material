# HDFS administration
[Link](https://hadoop.apache.org/docs/r2.7.5/hadoop-project-dist/hadoop-hdfs/HDFSCommands.html)

## Version
```
hdfs version
```
Example output:
```
Hadoop 2.7.3.2.6.4.0-91
Subversion git@github.com:hortonworks/hadoop.git -r a4b6e1c0e98b4488d38bcc0a241dcbe3538b1c4d
Compiled by jenkins on 2018-01-04T10:27Z
Compiled with protoc 2.5.0
From source with checksum 9c28f884302610b59b221c7fbaeac3e
This command was run using /usr/hdp/2.6.4.0-91/hadoop/hadoop-common-2.7.3.2.6.4.0-91.jar
```

## Report
```
sudo su hdfs -l -c 'hdfs dfsadmin -report'
```
Example output:
```
Configured Capacity: 66297835520 (61.74 GB)
Present Capacity: 15903269266 (14.81 GB)
DFS Remaining: 14194422162 (13.22 GB)
DFS Used: 1708847104 (1.59 GB)
DFS Used%: 10.75%
Under replicated blocks: 0
Blocks with corrupt replicas: 0
Missing blocks: 0
Missing blocks (with replication factor 1): 0

-------------------------------------------------
Live datanodes (1):

Name: 172.17.0.2:50010 (sandbox-hdp.hortonworks.com)
Hostname: sandbox-hdp.hortonworks.com
Decommission Status : Normal
Configured Capacity: 66297835520 (61.74 GB)
DFS Used: 1708847104 (1.59 GB)
Non DFS Used: 46673522688 (43.47 GB)
DFS Remaining: 14194422162 (13.22 GB)
DFS Used%: 2.58%
DFS Remaining%: 21.41%
Configured Cache Capacity: 0 (0 B)
Cache Used: 0 (0 B)
Cache Remaining: 0 (0 B)
Cache Used%: 100.00%
Cache Remaining%: 0.00%
Xceivers: 6
Last contact: Mon Sep 17 08:48:15 UTC 2018
Last Block Report: Mon Sep 17 08:33:13 UTC 2018
```

## Print cluster topology
```
sudo su hdfs -l -c 'hdfs dfsadmin -printTopology'
```
Example output:
```
Rack: /default-rack
   172.17.0.2:50010 (sandbox-hdp.hortonworks.com)
```

## Balancer
Rebalances the datanode's blocks.
[Guide+Design](https://issues.apache.org/jira/browse/HADOOP-1652)
```
sudo su hdfs -l -c 'hdfs balancer'
```
Example output:
```
18/09/17 08:49:40 INFO balancer.Balancer: namenodes  = [hdfs://sandbox-hdp.hortonworks.com:8020]
18/09/17 08:49:40 INFO balancer.Balancer: parameters = Balancer.BalancerParameters [BalancingPolicy.Node, threshold = 10.0, max idle iteration = 5, #excluded nodes = 0, #included nodes = 0, #source nodes = 0, #blockpools = 0, run during upgrade = false]
18/09/17 08:49:40 INFO balancer.Balancer: included nodes = []
18/09/17 08:49:40 INFO balancer.Balancer: excluded nodes = []
18/09/17 08:49:40 INFO balancer.Balancer: source nodes = []
Time Stamp               Iteration#  Bytes Already Moved  Bytes Left To Move  Bytes Being Moved
18/09/17 08:49:41 INFO balancer.KeyManager: Block token params received from NN: update interval=10hrs, 0sec, token lifetime=10hrs, 0sec
18/09/17 08:49:41 INFO block.BlockTokenSecretManager: Setting block keys
18/09/17 08:49:41 INFO balancer.KeyManager: Update block keys every 2hrs, 30mins, 0sec
18/09/17 08:49:41 INFO balancer.Balancer: dfs.balancer.movedWinWidth = 5400000 (default=5400000)
18/09/17 08:49:41 INFO balancer.Balancer: dfs.balancer.moverThreads = 1000 (default=1000)
18/09/17 08:49:41 INFO balancer.Balancer: dfs.balancer.dispatcherThreads = 200 (default=200)
18/09/17 08:49:41 INFO balancer.Balancer: dfs.datanode.balance.max.concurrent.moves = 5 (default=5)
18/09/17 08:49:41 INFO balancer.Balancer: dfs.balancer.getBlocks.size = 2147483648 (default=2147483648)
18/09/17 08:49:41 INFO balancer.Balancer: dfs.balancer.getBlocks.min-block-size = 10485760 (default=10485760)
18/09/17 08:49:41 INFO block.BlockTokenSecretManager: Setting block keys
18/09/17 08:49:41 INFO balancer.Balancer: dfs.balancer.max-size-to-move = 10737418240 (default=10737418240)
18/09/17 08:49:41 INFO balancer.Balancer: dfs.blocksize = 134217728 (default=134217728)
18/09/17 08:49:41 INFO net.NetworkTopology: Adding a new node: /default-rack/172.17.0.2:50010
18/09/17 08:49:41 INFO balancer.Balancer: 0 over-utilized: []
18/09/17 08:49:41 INFO balancer.Balancer: 0 underutilized: []
The cluster is balanced. Exiting...
Sep 17, 2018 8:49:41 AM           0                  0 B                 0 B                0 B
Sep 17, 2018 8:49:42 AM  Balancing took 1.75 seconds

```

## Check for filesystem inconsistencies
```
hdfs fsck /hdfs/path
```
Example output:
```
Connecting to namenode via http://sandbox-hdp.hortonworks.com:50070/fsck?ugi=root&path=%2F
FSCK started by root (auth:SIMPLE) from /172.17.0.2 for path / at Mon Sep 17 08:50:24 UTC 2018
....................................................................................................
....................................................................................................
....................................................................................................
....................................................................................................
....................................................................................................
....................................................................................................
....................................................................................................
....................................................................................................
..............Status: HEALTHY
 Total size:	1680691179 B (Total open files size: 448 B)
 Total dirs:	237
 Total files:	814
 Total symlinks:		0 (Files currently being written: 6)
 Total blocks (validated):	812 (avg. block size 2069816 B) (Total open file blocks (not validated): 5)
 Minimally replicated blocks:	812 (100.0 %)
 Over-replicated blocks:	0 (0.0 %)
 Under-replicated blocks:	0 (0.0 %)
 Mis-replicated blocks:		0 (0.0 %)
 Default replication factor:	1
 Average block replication:	1.0
 Corrupt blocks:		0
 Missing replicas:		0 (0.0 %)
 Number of data-nodes:		1
 Number of racks:		1
FSCK ended at Mon Sep 17 08:50:24 UTC 2018 in 250 milliseconds


The filesystem under path '/' is HEALTHY
```

## Check files blocks metadata
```
hdfs fsck /hdfs/path -files -blocks -locations
```
Example output:
```
Connecting to namenode via http://sandbox-hdp.hortonworks.com:50070/fsck?ugi=root&files=1&blocks=1&locations=1&path=%2Ftmp
FSCK started by root (auth:SIMPLE) from /172.17.0.2 for path /tmp at Mon Sep 17 08:52:18 UTC 2018
/tmp <dir>
/tmp/entity-file-history <dir>
/tmp/entity-file-history/active <dir>
/tmp/hive <dir>
/tmp/hive/ambari-qa <dir>
/tmp/hive/hive <dir>
/tmp/hive/hive/2ef7a6b8-1558-4814-a518-fa73f57d84c4 <dir>
/tmp/hive/hive/2ef7a6b8-1558-4814-a518-fa73f57d84c4/_tmp_space.db <dir>
/tmp/hive/hive/33a1afa6-eb9a-45ce-bc0c-9c2487a7c058 <dir>
/tmp/hive/hive/33a1afa6-eb9a-45ce-bc0c-9c2487a7c058/_tmp_space.db <dir>
/tmp/hive/hive/5ddef836-5c2b-4d7c-b03f-fee884d16299 <dir>
/tmp/hive/hive/5ddef836-5c2b-4d7c-b03f-fee884d16299/_tmp_space.db <dir>
/tmp/hive/hive/6758da44-10d7-40bf-8414-9fa4b6e9796a <dir>
/tmp/hive/hive/6758da44-10d7-40bf-8414-9fa4b6e9796a/_tmp_space.db <dir>
/tmp/hive/hive/83d5bdf7-f498-4479-936b-af3df302fef1 <dir>
/tmp/hive/hive/83d5bdf7-f498-4479-936b-af3df302fef1/_tmp_space.db <dir>
/tmp/hive/hive/8e95e126-7dc1-4c62-ae45-979e037089e8 <dir>
/tmp/hive/hive/8e95e126-7dc1-4c62-ae45-979e037089e8/_tmp_space.db <dir>
/tmp/hive/hive/928d37a4-b8ca-4a0f-87df-89d9fda1507f <dir>
/tmp/hive/hive/928d37a4-b8ca-4a0f-87df-89d9fda1507f/_tmp_space.db <dir>
/tmp/hive/hive/_tez_session_dir <dir>
/tmp/hive/hive/c2c0591b-247b-4b64-9a86-725a6ccbe120 <dir>
/tmp/hive/hive/c2c0591b-247b-4b64-9a86-725a6ccbe120/_tmp_space.db <dir>
/tmp/hive/hive/d7ff44a6-f8d3-4a9d-99ec-f9b406eb6294 <dir>
/tmp/hive/hive/d7ff44a6-f8d3-4a9d-99ec-f9b406eb6294/_tmp_space.db <dir>
/tmp/hive/hive/f3b0257c-bf74-456b-90f1-53ba2a2e58dc <dir>
/tmp/hive/hive/f3b0257c-bf74-456b-90f1-53ba2a2e58dc/_tmp_space.db <dir>
Status: HEALTHY
 Total size:	0 B
 Total dirs:	27
 Total files:	0
 Total symlinks:		0
 Total blocks (validated):	0
 Minimally replicated blocks:	0
 Over-replicated blocks:	0
 Under-replicated blocks:	0
 Mis-replicated blocks:		0
 Default replication factor:	1
 Average block replication:	0.0
 Corrupt blocks:		0
 Missing replicas:		0
 Number of data-nodes:		1
 Number of racks:		1
FSCK ended at Mon Sep 17 08:52:18 UTC 2018 in 5 milliseconds


The filesystem under path '/tmp' is HEALTHY
```

## Enter safe mode (read-only)
```
sudo su hdfs -l -c 'hdfs dfsadmin -safemode enter'
```

## Exit safe mode
```
sudo su hdfs -l -c 'hdfs dfsadmin -safemode leave'
```

## Create checkpoint
```
sudo su hdfs -l -c 'hdfs dfsadmin -saveNamespace'
```
Must run in Safe Mode

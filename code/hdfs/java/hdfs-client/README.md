# HDFS Java Client

## Arguments
* ```{url}:50070```
* path to local file
* HDFS directory

A simple example of a java program that connects to an HDFS cluster and lists the root directories.

The program takes as command line argument the HDFS url. If it isn't specified then ```hdfs://127.0.0.1:8020``` is used.

Maven produces a normal ```jar``` and an ```uber jar``` (with dependencies included).

## Execute from local machine
1. Execute the uber jar
    ```
    java -jar hdfs-client/target/uber-hdfs-client-1.0-SNAPSHOT.jar /some/file.txt /tmp
    ```

Note that this assumes that the HDFS cluster is accessible through the ```hdfs://127.0.0.1:8020``` url. If this is not the case define the url as command line argument.

## Hortonworks HDP sandbox execution
This example uses the docker image hortonworks/sandbox-hdp-standalone:2.6.4

1. Copy the jar to the cluster
    ```
    scp -P 2222 hdfs-client/target/hdfs-client-1.0-SNAPSHOT.jar maria_dev@localhost:tmp
    ```
1. Login through ssh to HDP
    ```
    ssh maria_dev@localhost -p 2222
    ```
1. Execute the jar through hadoop command
    ```
    hadoop jar tmp/hdfs-client-1.0-SNAPSHOT.jar "hdfs://sandbox-hdp.hortonworks.com:8020" /some/file.txt /tmp
    ```

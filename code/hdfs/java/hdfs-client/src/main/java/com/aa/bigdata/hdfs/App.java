package com.aa.bigdata.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.*;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main(String[] args) {
        String url;
        String sourceFile;
        String hdfsDestDir;
        if(args.length == 3 || args.length == 1) {
            url = args[0];
            sourceFile = args[1];
            hdfsDestDir = args[2];
        } else if (args.length == 2){
            url = "hdfs://127.0.0.1:8020";
            sourceFile = args[0];
            hdfsDestDir = args[1];
        } else {
            System.out.println("Source and destination paths are not defined");
            return;
        }

        System.out.println("Connect to HDFS: " + url);
        Configuration conf = new Configuration();
        conf.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
        conf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
        conf.set("dfs.client.use.datanode.hostname", "true"); //Remove this in case of a cluster
        conf.set("fs.defaultFS", url);
        listFiles(conf);
        putFile(conf, sourceFile, hdfsDestDir);
    }

    static void listFiles(Configuration conf) {
        try {
            FileSystem fs = FileSystem.get(conf);

            FileStatus[] fsStatus = fs.listStatus(new Path("/"));
            for (int i = 0; i < fsStatus.length; i++) {
                System.out.println(fsStatus[i].getPath().toString());
            }
        }catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException EXCEPTION: " + e);
        }catch (IOException e) {
            System.out.println("IO EXCEPTION: " + e);
        }catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentException EXCEPTION: " + e);
        }
    }

    static void putFile(Configuration conf, String source, String dest) {
        try{
            FileSystem fileSystem = FileSystem.get(conf);
            String filename = source.substring(source.lastIndexOf('/') + 1,source.length());

            if (dest.charAt(dest.length() - 1) != '/') {
                dest = dest + "/" + filename;
            } else {
                dest = dest + filename;
            }

            Path path = new Path(dest);
            if (fileSystem.exists(path)) {
                System.out.println("File " + dest + " already exists");
                return;
            }

            // Create a new file and write data to it.
            FSDataOutputStream out = fileSystem.create(path);
            InputStream in = new BufferedInputStream(new FileInputStream(new File(source)));

            byte[] b = new byte[1024];
            int numBytes = 0;
            while ((numBytes = in.read(b)) > 0) {
                out.write(b, 0, numBytes);
            }

            // Close all the file descriptors
            in.close();
            out.close();
            fileSystem.close();
        }catch(IOException e) {
            System.out.println("IO EXCEPTION: " + e);
        }
    }
}

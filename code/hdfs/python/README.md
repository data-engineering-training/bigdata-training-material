# Python HDFS Client

## Arguments
* ```{url}:50070```
* path to local file
* HDFS directory

### Example
```
python pyhdfs "localhost:50070" /some/file.txt /tmp
```

## Docker

### Build image
```
docker build -t pyhdfs .
```

### Execute
```
docker run -it --rm --network=host pyhdfs
```

### Execute (for development)
This option mounts the source code from the host file system. Code changes can be tested without rebuilding the image
```
docker run -it \
  --rm \
  --network=host \
  -v /path/to/bigdata-training-material/code/hdfs/python/source:/usr/src/app/source \
  pyhdfs
```

import sys
import pyhdfs

def main():
    if len(sys.argv) < 4:
        print("The following arguments are required: HDFS_HOST LOCAL_FILE_PATH HDFS_PATH")
        exit(1)
    hostname = sys.argv[1] # 'localhost:50070'
    source = sys.argv[2]
    dest = sys.argv[3] if sys.argv[3].endswith('/') else sys.argv[3] + '/'
    filename = source.split('/')[-1]
    hdfs = pyhdfs.HdfsClient(hosts=hostname)

    try:
        dirs = hdfs.listdir('/')
        print("HDFS Directories: ", dirs)
    except pyhdfs.HdfsFileNotFoundException as e:
        print("Directory doesn't exist")
    except pyhdfs.HdfsException as e:
        print("HDFS error")

    sourcefile = open(source, 'r')
    try:
        hdfs.create(dest + filename, sourcefile)
        print("HDFS file successfully created")
    except pyhdfs.HdfsFileAlreadyExistsException as e:
        print("File already exists")
    except pyhdfs.HdfsException as e:
        print("HDFS error")

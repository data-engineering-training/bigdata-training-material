from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.postgres_operator import PostgresOperator
from datetime import datetime, timedelta
import psycopg2 as pg
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from googleapiclient.http import MediaIoBaseDownload
import io
import csv
import pandas as pd


default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": datetime(2018, 6, 30),
    "email": ["youremail@email.com"],
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}

dag = DAG("update_sale_statuses", default_args=default_args, schedule_interval='@daily', catchup=False)


def download_csv(ds, **kwargs):

    # Setup the Drive v3 API
    SCOPES = 'https://www.googleapis.com/auth/drive'
    store = file.Storage('/home/pacuna/airflow/dags/sales/credentials.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets('/home/pacuna/airflow/dags/sales/client_secret.json', SCOPES)
        creds = tools.run_flow(flow, store)
    service = build('drive', 'v3', http=creds.authorize(Http()))

    file_id = '1kYN65lVCk7Zu3OC-tocJ9Xm8Iyq73hT56iyXCGLz-DU'
    request = service.files().export_media(fileId=file_id,
                                                 mimeType='text/csv')
    fh = io.FileIO('/home/pacuna/airflow/dags/sales/statuses.csv', 'wb')
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
        print("Download %d%%." % int(status.progress() * 100))

def select_columns(ds, **kwargs):
    df = pd.read_csv("/home/pacuna/airflow/dags/sales/statuses.csv")
    filtered_df = df[df['sale_id'].notnull()]
    filtered_df['updated_at'] = 'now()'
    filtered_df[['sale_id', 'status', 'updated_at']].to_csv('/home/pacuna/airflow/dags/sales/final.csv', index=False, header=False)

def load_to_dwh(ds, **kwargs):
    conn = pg.connect("host=localhost dbname=postgres user=postgres password=mysecretpassword")
    cur = conn.cursor()
    f = open(r'/home/pacuna/airflow/dags/sales/final.csv', 'r')
    f.seek(0)
    cur.copy_from(f, 'sale_statuses', sep=',')
    conn.commit()
    f.close()

t1 = PythonOperator(
    task_id='extract_from_drive',
    provide_context=True,
    python_callable=download_csv,
    dag=dag)

t2 = PythonOperator(
    task_id='select_columns',
    provide_context=True,
    python_callable=select_columns,
    dag=dag)

t3 = PostgresOperator(
    task_id='truncate_dwh_table',
    postgres_conn_id="dwh",
    sql="TRUNCATE table sale_statuses",
    database="postgres",
    dag=dag)

t4 = PythonOperator(
    task_id='load_to_dwh',
    provide_context=True,
    python_callable=load_to_dwh,
    dag=dag)

t2.set_upstream(t1)
t3.set_upstream(t2)
t4.set_upstream(t3)
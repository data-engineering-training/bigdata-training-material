import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.streaming.Trigger;
import org.apache.spark.sql.types.StructType;

import java.util.concurrent.TimeUnit;

public class TaxiStreamingJoin {
    public static void main(String[] args) {

        SparkSession spark = SparkSession
                .builder()
                .appName("TaxiStreamingJoin")
                .getOrCreate();

        Dataset<Row> taxiZoneLookup = spark.read().format("csv")
                .option("sep", ",")
                .option("inferSchema", "true")
                .option("header", "true")
                .load("C:\\Users\\Pantelis Nasikas\\Downloads\\nyc_taxi_data\\taxi_zone_lookup.csv");

        taxiZoneLookup.printSchema();

        taxiZoneLookup.show(50);

        StructType taxiSchema = new NycTaxiTrip().getSchema();

        Dataset<Row> trips_csv_stream = spark.readStream().option("maxFilesPerTrigger","1")
                .schema(taxiSchema)
                .csv("C:\\Users\\Pantelis Nasikas\\Downloads\\nyc_taxi_data\\trips_csv\\chunks\\");

        trips_csv_stream.printSchema();

        Dataset<Row> trips_pt_query = trips_csv_stream
                .join(taxiZoneLookup,
                        trips_csv_stream.col("PULocationID").eqNullSafe(taxiZoneLookup.col("LocationID")))
                .withWatermark("tpep_pickup_datetime", "20 minutes")
                .groupBy(
                        functions.window( functions.col("tpep_pickup_datetime"), "15 minutes"),
                        functions.col("Borough")
                )
                .agg(functions.sum("trip_distance").alias("trip_distance_sum"),
                        functions.sum("total_amount").alias("total_amount_sum"))
                .select(functions.col("window.start").alias("start_window"),
                        functions.col("window.end").alias("stop_window"),
                        functions.col("Borough"),
                        functions.col("trip_distance_sum"),
                        functions.col("total_amount_sum"));

        trips_pt_query.printSchema();

        StreamingQuery query = trips_pt_query.writeStream()
//                .format("console")
                .outputMode("append")
                .option("path", "C:\\temp\\pt_ta_per_pc4\\aggjoin")
                .option("checkpointLocation","C:\\temp\\spark_checkpoints\\aggjoin")
                .format("csv")
                .trigger(Trigger.ProcessingTime(30, TimeUnit.SECONDS))
                .start();

        try {
            query.awaitTermination();
        } catch (StreamingQueryException e) {
            e.printStackTrace();
        } finally {
            spark.close();
        }
    }

}

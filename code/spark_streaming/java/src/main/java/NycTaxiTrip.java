import org.apache.spark.sql.types.StructType;

public class NycTaxiTrip {
    public StructType getSchema() {
        return new StructType().add("VendorID", "integer")
                .add("tpep_pickup_datetime", "timestamp")
                .add("tpep_dropoff_datetime", "timestamp")
                .add("passenger_count", "integer")
                .add("trip_distance", "float")
                .add("RatecodeID", "int")
                .add("store_and_fwd_flag", "string")
                .add("PULocationID", "integer")
                .add("DOLocationID", "integer")
                .add("payment_type", "integer")
                .add("fare_amount", "float")
                .add("extra", "float")
                .add("mta_tax", "float")
                .add("tip_amount", "float")
                .add("tolls_amount", "float")
                .add("improvement_surcharge", "float")
                .add("total_amount", "float");

    }
}

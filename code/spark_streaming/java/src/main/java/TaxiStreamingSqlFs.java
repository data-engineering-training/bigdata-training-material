import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.types.StructType;


public class TaxiStreamingSqlFs {

    public static void main(String[] args) {

        SparkSession spark = SparkSession
                .builder()
                .appName("TaxiStreamingSqlFs")
                .getOrCreate();

        StructType taxiSchema = new NycTaxiTrip().getSchema();

        Dataset<Row> trips_csv_stream = spark.readStream().option("maxFilesPerTrigger","1")
                    .schema(taxiSchema)
                    .csv("C:\\Users\\Pantelis Nasikas\\Downloads\\nyc_taxi_data\\trips_csv\\chunks\\");

        trips_csv_stream.createOrReplaceTempView("nyctaxitrips");

        Dataset<Row> trips_pt_query = spark.sql("select passenger_count , payment_type , total_amount " +
                "from nyctaxitrips where passenger_count > 4");

        StreamingQuery query = trips_pt_query.writeStream()
                .outputMode("append")
                .format("csv")        // can be "orc", "json", "csv", etc.
                .option("path", "C:\\temp\\pt_ta_per_pc4\\sqlfs")
                .option("checkpointLocation","C:\\temp\\spark_checkpoints\\sqlfs")
                .start();

        try {
            query.awaitTermination();
        } catch (StreamingQueryException e) {
            e.printStackTrace();
        } finally {
            spark.close();
        }
    }

}

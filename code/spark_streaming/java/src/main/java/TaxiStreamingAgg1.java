import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.streaming.Trigger;
import org.apache.spark.sql.types.StructType;

import java.util.concurrent.TimeUnit;

public class TaxiStreamingAgg1 {
    public static void main(String[] args) {

        SparkSession spark = SparkSession
                .builder()
                .appName("TaxiStreamingAgg1")
                .getOrCreate();

        StructType taxiSchema = new NycTaxiTrip().getSchema();

        Dataset<Row> trips_csv_stream = spark.readStream().option("maxFilesPerTrigger","1")
                .schema(taxiSchema)
                .csv("C:\\Users\\Pantelis Nasikas\\Downloads\\nyc_taxi_data\\trips_csv\\chunks\\");

        Dataset<Row> trips_pt_query = trips_csv_stream
                .withWatermark("tpep_pickup_datetime", "10 minutes")
                .groupBy(
                        functions.window( functions.col("tpep_pickup_datetime"), "10 minutes"),
                        functions.col("VendorID")
                )
                .agg(functions.sum("trip_distance").alias("trip_distance_sum"),
                        functions.sum("total_amount").alias("total_amount_sum"))
                .select(functions.col("window.start").alias("start_window"),
                        functions.col("window.end").alias("stop_window"),
                        functions.col("VendorID"),
                        functions.col("trip_distance_sum"),
                        functions.col("total_amount_sum"));

        trips_pt_query.printSchema();

        StreamingQuery query = trips_pt_query.writeStream()
                .outputMode("append")
                .option("path", "C:\\temp\\pt_ta_per_pc4\\agg1")
                .option("checkpointLocation","C:\\temp\\spark_checkpoints\\agg1")
                .format("csv")
                .trigger(Trigger.ProcessingTime(30, TimeUnit.SECONDS))
                .start();

        try {
            query.awaitTermination();
        } catch (StreamingQueryException e) {
            e.printStackTrace();
        } finally {
            spark.close();
        }
    }

}

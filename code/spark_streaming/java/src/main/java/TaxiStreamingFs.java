import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.types.StructType;

import java.util.Arrays;
import java.util.Iterator;


public class TaxiStreamingFs {

    public static void main(String[] args) {

        SparkSession spark = SparkSession
                .builder()
                .appName("TaxiFsStreaming")
                .getOrCreate();

        StructType taxiSchema = new NycTaxiTrip().getSchema();

        Dataset<Row> trips_csv_stream = spark.readStream().option("maxFilesPerTrigger","1")
                    .schema(taxiSchema)
                    .csv("C:\\Users\\Pantelis Nasikas\\Downloads\\nyc_taxi_data\\trips_csv\\chunks\\");

        Dataset<Row> trips_pt_query = trips_csv_stream.select("passenger_count","payment_type", "total_amount")
                .where("passenger_count > 4");

        StreamingQuery query = trips_pt_query.writeStream()
                .outputMode("append")
                .format("csv")
                .option("path", "C:\\temp\\pt_ta_per_pc4")
                .option("checkpointLocation","C:\\temp\\spark_checkpoints\\basicfs")
//                .format("console")
                .start();

        try {
            query.awaitTermination();
        } catch (StreamingQueryException e) {
            e.printStackTrace();
        } finally {
            spark.close();
        }
    }

}

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.StreamingQuery;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.concat;
//import static org.apache.spark.sql.functions.

import java.util.Arrays;
import java.util.Iterator;


public class FillTaxiTripsTopic {

    public static void main(String[] args) {

        SparkSession spark = SparkSession
                .builder()
                .appName("FillTaxiTripsTopic")
                .getOrCreate();


        Dataset<Row> nycTaxiDFCsv = spark.read().format("csv")
                .option("sep", ";")
                .option("inferSchema", "true")
                .option("header", "true")
                .load("C:\\Users\\Pantelis Nasikas\\Downloads\\nyc_taxi_data\\trips_csv\\100_yellow_tripdata_2017-01.csv");

//        nycTaxiDFCsv.select(col("VendorID,tpep_pickup_datetime,tpep_dropoff_datetime,passenger_count,trip_distance,RatecodeID,store_and_fwd_flag,PULocationID,DOLocationID,payment_type,fare_amount,extra,mta_tax,tip_amount,tolls_amount,improvement_surcharge,total_amount").alias("value"))
//                .show();

        nycTaxiDFCsv.printSchema();

//        nycTaxiDFCsv.selectExpr("cast(VendorID as STRING),")

        nycTaxiDFCsv.select(col("VendorID,tpep_pickup_datetime,tpep_dropoff_datetime,passenger_count,trip_distance,RatecodeID,store_and_fwd_flag,PULocationID,DOLocationID,payment_type,fare_amount,extra,mta_tax,tip_amount,tolls_amount,improvement_surcharge,total_amount").alias("value")).write()
                .format("kafka")
                .option("kafka.bootstrap.servers", "10.9.43.246:9092")
                .option("topic", "nyctaxialltrips")
                .save();


    }
}
